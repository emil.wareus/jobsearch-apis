FROM  docker-images.jobtechdev.se/mirror/python:3.9.9-slim-bullseye

EXPOSE 8081

ENV FLASK_APP=jobsearch \
    TZ=Europe/Stockholm \
    PROC_NR=8 \
    HARAKIRI=120

RUN apt-get update -y && \
    apt-get install -y \
    git \
    build-essential && \
    apt-get clean -y && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . /app

WORKDIR /app

RUN python -m pip install --upgrade setuptools wheel pip && \
    pip install -r requirements.txt && \
    python -m pytest -svv tests/unit_tests && \
    rm -rf tests && \
    echo "" && echo Application: $FLASK_APP && echo Process total: $PROC_NR && echo ""

CMD  uwsgi --http :8081 --manage-script-name --mount /=${FLASK_APP:-jobsearch}:app --ini uwsgi.ini

# TODO:
# Have a separate build step copy only necessary files to final image to get a small one.
