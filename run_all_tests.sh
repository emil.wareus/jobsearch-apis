
function stop_flask {
  # find process id of process using port 5000 and kill it
  process_id=$(lsof -i:5000 -t)
  kill -9 $process_id
  # todo: check instead of sleep
  sleep 5
}

function start_flask {
  # start flask and wait for it to be running
  flask run &
  # todo: check instead of sleep
  sleep 5
}


# set env vars
source local.env

# run unit tests
pytest tests/unit_tests
unit_test_result=$?

if [ "$unit_test_result" != 0 ]; then
  # no point in continuing if unit tests fail
  echo Unit tests failed, exiting
  exit 1
fi

# run integration tests
pytest tests/integration_tests
integration_test_result=$?


# jobsearch
export FLASK_APP=jobsearch
start_flask
# run jobsearch api tests
pytest tests/api_tests/searching/common
jobsearch_common_result=$?
pytest tests/api_tests/searching/jobsearch
jobsearch_results=$?
stop_flask

# historical ads
export FLASK_APP=historical
start_flask
# run api tests
pytest tests/api_tests/searching/common
historical_common_result=$?
pytest tests/api_tests/searching/historical
historical_result=$?
stop_flask

# jobstream
export FLASK_APP=jobstream
start_flask
# run api tests
pytest tests/api_tests/jobstream
jobstream_result=$?
stop_flask

# results

echo Unit tests $unit_test_result
echo Integration tests $integration_test_result
echo Jobsearch common tests $jobsearch_common_result
echo Jobsearch tests $jobsearch_results
echo Historical common tests $historical_common_result
echo Historical tests $historical_result
echo Jobstream tests $jobstream_result


# todo:
# test reports
# run with coverage and create reports for each part
# check instead of sleep
