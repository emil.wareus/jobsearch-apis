import pytest
from requests.exceptions import HTTPError, ConnectionError
from common.settings import START_SEASONAL_TIME, END_SEASONAL_TIME
from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import NUMBER_OF_ADS, TEST_USE_STATIC_DATA

pytestmark = pytest.mark.historical


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_mm_dd, to_mm_dd, expected", [
    ('02-28', '05-31', 3809),
    ('02-28', '03-01', 110),
    ('01-01', '12-31', NUMBER_OF_ADS),
    ('01-01', '06-30', 4875),
    ('05-31', '10-10', 7),
    ('1-1', '12-31', NUMBER_OF_ADS),
    ('2-2', '5-9', 4611)])
def test_historical_season_filter(session, from_mm_dd, to_mm_dd, expected):
    params = {START_SEASONAL_TIME: from_mm_dd, END_SEASONAL_TIME: to_mm_dd}
    result = get_search(session, params)
    assert result['total']['value'] == expected, params


@pytest.mark.skip("needs both params")
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("start_mm_dd,  expected", [
    ('03-01', -1),
    ('02-28', -1),
    ('01-01', -1),
    ('12-31', -1),
    ('07-30', -1),
])
def test_historical_season_filter_start_date(session, start_mm_dd, expected):
    params = {START_SEASONAL_TIME: start_mm_dd}
    result = get_search(session, params)
    assert result['total']['value'] == expected, params


@pytest.mark.skip("needs both params")
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("end_mmdd, expected", [
    ('02-01', -1),
    ('01-20', -1),
    ('01-10', -1),
    ('12-31', -1),
    ('01-01', -1),
])
def test_historical_season_filter_end_date(session, end_mmdd, expected):
    params = {END_SEASONAL_TIME: end_mmdd}
    result = get_search(session, params)
    assert result['total']['value'] == expected, params


@pytest.mark.parametrize("from_mm_dd, to_mm_dd", [
    ('02-29', '05-31'),
    ('01-01', '02-29'),
    ('08-32', '11-29'),
])
def test_error(session, from_mm_dd, to_mm_dd):
    """
  February 29 only exists on leap years, causing an error when trying to use it for
  this filter that checks every year
  Dates that do not exist ever also raises an error
    """
    params = {END_SEASONAL_TIME: from_mm_dd, START_SEASONAL_TIME: to_mm_dd}
    with pytest.raises((HTTPError, ConnectionError)):
        get_search(session, params)
