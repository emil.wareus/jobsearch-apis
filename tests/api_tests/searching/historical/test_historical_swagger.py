import json
import pytest
from common import settings
from tests.test_resources.test_settings import TEST_URL


@pytest.mark.historical
def test_search_swagger_historical(session):
    """
    Test Swagger info for historical ads
    """
    response = session.get(f"{TEST_URL}/swagger.json")
    response.raise_for_status()
    response_json = json.loads(response.content)
    assert response_json['info']['version'] == settings.API_VERSION

    expected = [('/ad/{id}', 3), ('/search', 2), ]
    for expected_path, expected_number in expected:
        assert response_json['paths'][expected_path]
        assert len(response_json['paths'][expected_path]['get']['responses']) == expected_number
