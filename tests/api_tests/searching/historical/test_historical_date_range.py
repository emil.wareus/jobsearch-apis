import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import NUMBER_OF_ADS, TEST_USE_STATIC_DATA
from common.settings import HISTORICAL_FROM, HISTORICAL_TO

@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on mock data")
@pytest.mark.parametrize("from_date, to_date, expected", [

    (None, None, NUMBER_OF_ADS),
    ('2021-03', None, 3799),
    ('2021-02', None, 4640),
    ('2021-02', '2021-03', 4635),
    (None, '2021-02', 1230),
    ('2019', '2020', 154),
    ('2020-12', '2021-01', 324),
    (None, '2020', 154),
])
def test_historical_date(session, from_date, to_date, expected):
    if from_date and not to_date:
        params = {HISTORICAL_FROM: from_date}
    elif to_date and not from_date:
        params = {HISTORICAL_TO: to_date}
    elif from_date and to_date:
        params = {HISTORICAL_FROM: from_date, HISTORICAL_TO: to_date}
    else:
        params = {}

    result = get_search(session, params)
    assert result['total']['value'] == expected
