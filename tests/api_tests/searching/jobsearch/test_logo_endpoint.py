import pytest

from tests.test_resources.helper import get_logo

pytestmark = pytest.mark.jobsearch


def test_logo(session):
    """
    Try to get a logo from an ad that is in the test data set
    the logo png file comes from an external system
    """
    logo_response = get_logo(ad_id='24681267')
    assert "PNG" in logo_response.text
    assert logo_response.headers['Content-Type'] == 'image/png'
    assert 'logo.png' in str(logo_response.headers)


def test_logo_ad_not_found():
    response = get_logo(ad_id=112233, verify_response_ok=False)
    assert response.status_code == 404
    assert '"message": "Ad not found. You have requested this URI [/ad/112233/logo] but did you mean /ad/<id>/logo ?"' in response.text
