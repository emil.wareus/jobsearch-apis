import pytest

from tests.test_resources.helper import get_search, compare

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]

test_data = [
    ('murar*', 1),
    ('systemutvecklar*', 28),
    ('*utvecklare', 183),
    ('utvecklare*', 93),
    ('*utvecklare*', 0),  # double wildcard does not work
    ('Anläggningsarbetar*', 6),
    ('Arbetsmiljöingenjö*', 1),
    ('Beläggningsarbetar*', 2),
    ('Behandlingsassisten*', 10),
    ('Bilrekonditionerar*', 3),
    ('Eventkoordinato*', 0),
    ('Fastighetsförvaltar*', 9),
    ('Fastighetsskötar*', 16),
    ('Fastighet*', 244),
    ('Kundtjänstmedarbetar*', 26),
    ('*undtjänstmedarbetare', 26),
    ('Kundtjänst*', 107),
    ('sjukskö*', 651),
    ('*sköterska', 711),
    ('sköterska*', 7),
    ('skötersk*', 11),
    ('sjukvårds*tion', 0),
    ('sj', 0),  # minimum 3 characters
    ('sj*', 0),  # minimum 3 characters
    ('sju*', 1095)
]


@pytest.mark.smoke
@pytest.mark.parametrize("query, expected_number_of_hits", test_data)
def test_wildcard_search_exact_match(session, query, expected_number_of_hits):
    """
    Test different wildcard queries
    check that the number of results is exactly as expected
    """
    response_json = get_search(session, {'q': query, 'limit': '0'})
    compare(response_json['total']['value'], expected_number_of_hits, msg=f"Query: {query}")


@pytest.mark.parametrize("query, minimum_number_of_hits", test_data)
def test_wildcard_search_minimum_match(session, query, minimum_number_of_hits):
    """
    Test different wildcard queries
    check that the number of results is equal to or higher than 'minimum_number_of_hits'
    """
    response_json = get_search(session, {'q': query, 'limit': '0'})
    compare(response_json['total']['value'], minimum_number_of_hits, msg=f"Query: {query}")
