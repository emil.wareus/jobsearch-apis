import pytest
import copy

from tests.test_resources.helper import get_search, get_search_with_headers
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA
from common.settings import X_FEATURE_FREETEXT_BOOL_METHOD
from tests.test_resources.test_settings import test_headers


@pytest.mark.jobsearch
@pytest.mark.historical
@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("query, expected, bool_method", [
    ('Bauhaus Kundtjänst', 0, 'and'),
    ('Bauhaus Kundtjänst', 94, 'or'),
    ('Bauhaus Kundtjänst', 94, None),
    ('Sirius crew', 0, 'and'),
    ('Sirius crew', 4, 'or'),
    ('Sirius crew', 4, None),
    ('lagstiftning anställning ', 17, 'and'),
    ('lagstiftning anställning ', 1066, 'or'),
    ('lagstiftning anställning ', 1066, None),
    ('TechBuddy uppdrag', 1, 'and'),
    ('TechBuddy uppdrag', 1403, 'or'),
    ('TechBuddy uppdrag', 1403, None),
])
def test_freetext_bool_method(session, query, expected, bool_method):
    """
    Test with 'or' & 'and' values for X_FEATURE_FREETEXT_BOOL_METHOD header flag
    Default value is 'OR' (used in test cases with None as param)
    Searches with 'or' returns more hits
    """
    params = {'q': query, 'limit': 0}
    # use default setting for X_FEATURE_FREETEXT_BOOL_METHOD == 'OR'
    if not bool_method:
        response_json = get_search(session, params)
    else:
        tmp_headers = copy.deepcopy(test_headers)
        tmp_headers[X_FEATURE_FREETEXT_BOOL_METHOD] = bool_method
        response_json = get_search_with_headers(params, tmp_headers)

    if TEST_USE_STATIC_DATA:
        assert response_json['total']['value'] == expected
