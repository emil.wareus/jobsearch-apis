import pytest

from common.settings import OCCUPATION, OCCUPATION_GROUP, OCCUPATION_FIELD, MUNICIPALITY
from tests.test_resources.concept_ids import occupation, occupation_group, occupation_field, concept_ids_geo as geo
from tests.test_resources.helper import get_total
from tests.test_resources.test_settings import NUMBER_OF_ADS
from common.settings import REMOTE, UNSPECIFIED_SWEDEN_WORKPLACE, ABROAD, PUBLISHED_BEFORE, PUBLISHED_AFTER, \
    EXPERIENCE_REQUIRED

TEST_DATE = "2020-12-10T23:59:59"
NUMBER_OF_REMOTE_ADS = 36
# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.parametrize("params, expected", [
    ({REMOTE: True, 'q': 'utvecklare'}, 2),
    ({REMOTE: False, 'q': 'utvecklare'}, 164),
    ({REMOTE: None, 'q': 'utvecklare'}, 166),
    ({REMOTE: True, 'q': 'säljare'}, 1),
    ({REMOTE: False, 'q': 'säljare'}, 224),
    ({REMOTE: None, 'q': 'säljare'}, 225),
    ({REMOTE: True, OCCUPATION: occupation.saljkonsulent}, 0),
    ({REMOTE: None, OCCUPATION: occupation.saljkonsulent}, 4),
    ({REMOTE: False, OCCUPATION: occupation.saljkonsulent}, 4),
    ({OCCUPATION: occupation.saljkonsulent}, 4),
    ({OCCUPATION: occupation.mjukvaruutvecklare}, 50),
    ({REMOTE: True, OCCUPATION: occupation.mjukvaruutvecklare}, 1),
    ({REMOTE: False, OCCUPATION: occupation.mjukvaruutvecklare}, 49),
])
def test_remote_occupation(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({REMOTE: True, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_}, 3),
    ({REMOTE: None, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_}, 233),
    ({REMOTE: False, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_}, 230),
    ({REMOTE: True, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_}, 2),
    ({REMOTE: None, OCCUPATION_GROUP: occupation_group.telefonforsaljare_m_fl_}, 75),
    ({REMOTE: True, OCCUPATION_GROUP: occupation_group.foretagssaljare}, 2),
    ({REMOTE: None, OCCUPATION_GROUP: occupation_group.foretagssaljare}, 164),
])
def test_remote_occupation_group(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({REMOTE: True, OCCUPATION_FIELD: occupation_field.data_it}, 10),
    ({REMOTE: None, OCCUPATION_FIELD: occupation_field.data_it}, 421),
    ({REMOTE: True, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, 6),
    ({REMOTE: None, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, 653),
    ({REMOTE: False, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, 647),
])
def test_remote_occupation_field(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({'q': 'Stockholm'}, 844),
    ({REMOTE: True, 'q': 'Stockholm'}, 13),
    ({REMOTE: False, 'q': 'Stockholm'}, 831),
    ({MUNICIPALITY: geo.stockholm}, 775),
    ({REMOTE: True, MUNICIPALITY: geo.stockholm}, 13),
    ({REMOTE: False, MUNICIPALITY: geo.stockholm}, 762),
])
def test_remote_municipality(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({'q': 'Stockholms Län'}, 1228),
    ({REMOTE: True, 'q': 'Stockholms Län'}, 14),
    ({REMOTE: False, 'q': 'Stockholms Län'}, 1214),
    ({'region': geo.stockholms_lan}, 1227),
    ({REMOTE: True, 'region': geo.stockholms_lan}, 14),
    ({REMOTE: False, 'region': geo.stockholms_lan}, 1213),
    ({REMOTE: None, 'region': geo.vastra_gotalands_lan}, 779),
    ({REMOTE: True, 'region': geo.vastra_gotalands_lan}, 3),
    ({REMOTE: False, 'region': geo.vastra_gotalands_lan}, 776),
    ({'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, 26),
    ({REMOTE: False, 'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, 26),
    ({REMOTE: True, 'region': geo.vastra_gotalands_lan, 'q': 'säljare'}, 0),
])
def test_remote_region(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({PUBLISHED_AFTER: TEST_DATE}, 4935),
    ({REMOTE: True, PUBLISHED_AFTER: TEST_DATE}, 32),
    ({PUBLISHED_BEFORE: TEST_DATE}, 94),
    ({REMOTE: True, PUBLISHED_BEFORE: TEST_DATE}, 4)
])
def test_remote_publish_date(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({ABROAD: True}, 30),
    ({ABROAD: False}, NUMBER_OF_ADS),
    ({REMOTE: True, ABROAD: True}, 0),
    ({REMOTE: True, ABROAD: False}, NUMBER_OF_REMOTE_ADS),  # abroad False does nothing
])
def test_abroad(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({UNSPECIFIED_SWEDEN_WORKPLACE: True}, 135),
    ({UNSPECIFIED_SWEDEN_WORKPLACE: False}, NUMBER_OF_ADS),
    ({REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: True}, 2),
    ({REMOTE: True, UNSPECIFIED_SWEDEN_WORKPLACE: False}, NUMBER_OF_REMOTE_ADS)
])
def test_unspecified_workplace(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize('params, expected', [
    ({EXPERIENCE_REQUIRED: True}, 3938),
    ({EXPERIENCE_REQUIRED: False}, 1091),
    ({REMOTE: True, EXPERIENCE_REQUIRED: True}, 31),
    ({REMOTE: False, EXPERIENCE_REQUIRED: True}, 3907),
    ({REMOTE: True, EXPERIENCE_REQUIRED: False}, 5),
    ({REMOTE: False, EXPERIENCE_REQUIRED: False}, 1086)
])
def test_experience(session, params, expected):
    """
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


@pytest.mark.parametrize("params, expected", [
    ({REMOTE: True, 'q': '-Stockholm'}, 23),
    ({REMOTE: True, MUNICIPALITY: f"-{geo.stockholm}"}, 23),
    ({REMOTE: True, 'region': f"-{geo.stockholms_lan}"}, 22),
    ({REMOTE: True, 'region': f"-{geo.vastra_gotalands_lan}"}, 33),
    ({REMOTE: True, 'region': f"-{geo.skane_lan}"}, 31),
    ({REMOTE: True, 'region': f"-{geo.norrbottens_lan}"}, 36),
])
def test_remote_negative_geography(session, params, expected):
    """
    Negative geographical parameters
    AND condition between REMOTE and other params
    """
    assert get_total(session, params) == expected, f"params: {params}"


def test_combination_municipality(session):
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    number_region = get_total(session, {'municipality': geo.stockholm})
    number_remote = get_total(session, {REMOTE: True, 'municipality': geo.stockholm})
    number_not_remote = get_total(session, {REMOTE: False, 'municipality': geo.stockholm})
    assert number_remote + number_not_remote == number_region


def test_combination_region(session):
    """
    numbers for REMOTE True + REMOTE False should add upp to numbers when not using REMOTE
    AND condition between REMOTE and other params
    """
    number_region = get_total(session, {'region': geo.stockholms_lan})
    number_remote = get_total(session, {REMOTE: True, 'region': geo.stockholms_lan})
    number_not_remote = get_total(session, {REMOTE: False, 'region': geo.stockholms_lan})
    assert number_remote + number_not_remote == number_region


# noinspection PyDictDuplicateKeys
# (duplicate dictionary keys are intentional for testing)

@pytest.mark.parametrize("param, expected", [
    ({REMOTE: False, REMOTE: True}, NUMBER_OF_REMOTE_ADS),  # noinspection
    ({REMOTE: True, REMOTE: False}, (NUMBER_OF_ADS - NUMBER_OF_REMOTE_ADS)),
    ({REMOTE: True, REMOTE: False, REMOTE: True}, NUMBER_OF_REMOTE_ADS)
])
def test_duplicate_remote_param(session, param, expected):
    """
    with duplicated params, value of last param is used
    """
    assert get_total(session, params=param) == expected
