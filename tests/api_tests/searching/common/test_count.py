import pytest
from tests.test_resources.helper import get_total
from tests.test_resources.test_cases_for_count import test_cases_param

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]

@pytest.mark.smoke
@pytest.mark.parametrize("param_expected", test_cases_param)
def test_count(session, param_expected):
    p = param_expected[0]
    expected = param_expected[1]
    number_of_hits = get_total(session, params=p)
    assert number_of_hits == expected, f"Expected {expected} but got {number_of_hits} for {p}"
