import pytest
from tests.test_resources.helper import get_search
from tests.test_resources.taxonomy_replace_helper import ad_ids_from_response
from tests.test_resources.concept_ids.municipality import get_muncipalities_with_hits, \
    get_ten_random_municipalities_with_hits

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.slow
@pytest.mark.parametrize("municipality", get_muncipalities_with_hits())
def test_municipality_all(session, municipality):
    _check_municipality(session, municipality)


@pytest.mark.smoke
@pytest.mark.parametrize("municipality", get_ten_random_municipalities_with_hits())
def test_municipality_smoke_random(session, municipality):
    _check_municipality(session, municipality)


def _check_municipality(session, municipality):
    concept_id = municipality['id']
    name = municipality['name']
    region_name = municipality['region_name']
    # print(f"{name} - {region_name} - code {municipality['code']}")
    response = get_search(session, {'municipality': concept_id})

    assert (hits := response['hits'])
    ad_ids = ad_ids_from_response(response)
    assert ad_ids.sort() == municipality['ad_ids'].sort()
    for hit in hits:
        expected_municip = municipality['code']
        actual_municip = hit['workplace_address']['municipality_code']
        assert actual_municip == expected_municip  # , f"Expected code '{}' but "
        assert hit['workplace_address']['region_code'] == municipality['region_code']
