# -*- coding: utf-8 -*-

import pytest

from tests.test_resources.helper import get_search, get_total
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA, OPEN_FOR_ALL_PHRASE, REMOTE_MATCH_PHRASES, \
    TRAINEE_PHRASES, LARLING_PHRASES, FRANCHISE_PHRASES, HIRE_WORKPLACE_PHRASES
from common.settings import OPEN_FOR_ALL, REMOTE, TRAINEE, LARLING, FRANCHISE, HIRE_WORK_PLACE

# marks all tests in this file as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on specific data set")
@pytest.mark.parametrize('phrase_param, expected_true, expected_false', [
    (OPEN_FOR_ALL, 152, 4877),
    (REMOTE, 36, 4993),
    (TRAINEE, 4, 5025),
    (LARLING, 4, 5025),
    (FRANCHISE, 7, 5022),
    (HIRE_WORK_PLACE, 2, 5027)
])
def test_phrase_param_number_of_ads(session, phrase_param, expected_true, expected_false):
    total_true = get_total(session, {phrase_param: True, 'limit': 0})
    total_false = get_total(session, {phrase_param: False, 'limit': 0})
    error_msg = f"Actual: {total_true} / {total_false}, expected: {expected_true} / {expected_false}"
    assert (total_true == expected_true) and (total_false == expected_false), error_msg


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on specific data set")
@pytest.mark.parametrize('phrase_param, query,  expected_true, expected_false', [
    (OPEN_FOR_ALL, 'utvecklare', 3, 163),
    (OPEN_FOR_ALL, 'mötesbokare', 2, 7),
    (OPEN_FOR_ALL, 'tekniker', 1, 26),
    (OPEN_FOR_ALL, 'säljare', 8, 217),
    (REMOTE, 'utvecklare', 2, 164),
    (REMOTE, 'mötesbokare', 0, 9),
    (REMOTE, 'tekniker', 0, 27),
    (REMOTE, 'säljare', 1, 224),
    (TRAINEE, 'traineeprogram', 0, 4),
    (TRAINEE, 'frisör', 1, 12),
    (TRAINEE, 'tekniker', 0, 27),
    (TRAINEE, 'säljare', 0, 225),
    (LARLING, 'lärling', 2, 2),
    (LARLING, 'frisör', 0, 13),
    (LARLING, 'snickare', 0, 32),
    (LARLING, 'rörmokare', 0, 3),
    (FRANCHISE, 'lärling', 0, 4),
    (FRANCHISE, 'frisör', 0, 13),
    (FRANCHISE, 'pressbyrån', 1, 0),
    (FRANCHISE, 'företagare', 1, 44),
    (HIRE_WORK_PLACE, 'frisör', 2, 11),
    (HIRE_WORK_PLACE, 'mötesbokare', 0, 9),
    (HIRE_WORK_PLACE, 'tekniker', 0, 27),
    (HIRE_WORK_PLACE, 'frisörstol',0, 0)
])
def test_phrase_query(session, phrase_param, query, expected_true, expected_false):
    total_true = get_total(session, {'q': query, phrase_param: True, 'limit': 0})
    total_false = get_total(session, {'q': query, phrase_param: False, 'limit': 0})
    error_msg = f"Actual: {total_true} / {total_false}, expected: {expected_true} / {expected_false}"
    assert (total_true == expected_true) and (total_false == expected_false), error_msg


@pytest.mark.parametrize('phrase_param, phrases', [
    (REMOTE, REMOTE_MATCH_PHRASES),
    (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
    (TRAINEE, TRAINEE_PHRASES),
    (LARLING, LARLING_PHRASES),
    (FRANCHISE, FRANCHISE_PHRASES),
    (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES)
])
@pytest.mark.parametrize('query', ['hemifrån', 'säljare', 'java', 'lärare', 'tekniker', 'öppen för alla', 'trainee'])
def test_query_content(session, phrase_param, phrases, query):
    """
    Check that ads have at least one phrase associated with "öppen för alla"
    in description or title when using 'open_for_all': True
    """
    successes = []
    result = get_search(session, {'q': query, phrase_param: True, 'limit': 100})
    hits = result['hits']
    for hit in hits:
        for phrase in phrases:
            if phrase in hit['description']['text'].lower() or phrase in hit['headline'].lower():
                successes.append(True)
                break
    assert len(successes) >= len(hits)


@pytest.mark.parametrize('phrase_param, phrases', [
    (REMOTE, REMOTE_MATCH_PHRASES),
    (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
    (TRAINEE, TRAINEE_PHRASES),
    (LARLING, LARLING_PHRASES),
    (FRANCHISE, FRANCHISE_PHRASES),
    (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES)
])
@pytest.mark.parametrize('bool_value', [True, False])
def test_phrases_binary(session, phrase_param, phrases, bool_value):
    params = {phrase_param: bool_value}
    response = get_search(session, params)
    for hit in response['hits']:
        text_to_check = f"{hit['description']['text']} {hit['headline']}".lower()
        assert bool_value == any(x in text_to_check for x in phrases)


@pytest.mark.parametrize("query", ['säljare', 'java', 'sjuksköterska', 'frisör'])
@pytest.mark.parametrize('phrase_param, phrases', [
    (REMOTE, REMOTE_MATCH_PHRASES),
    (OPEN_FOR_ALL, OPEN_FOR_ALL_PHRASE),
    (TRAINEE, TRAINEE_PHRASES),
    (LARLING, LARLING_PHRASES),
    (FRANCHISE, FRANCHISE_PHRASES),
    (HIRE_WORK_PLACE, HIRE_WORKPLACE_PHRASES)
])
@pytest.mark.parametrize('bool_value', [True, False])
def test_phrases_binary_query(session, query, phrase_param, phrases, bool_value):
    params = {'q': query, phrase_param: bool_value}
    response = get_search(session, params)
    for hit in response['hits']:
        text_to_check = f"{hit['description']['text']} {hit['headline']}".lower()
        assert bool_value == any(x in text_to_check for x in phrases)


@pytest.mark.parametrize('phrase_param', [REMOTE,
                                          OPEN_FOR_ALL,
                                          TRAINEE,
                                          LARLING,
                                          FRANCHISE,
                                          HIRE_WORK_PLACE])
def test_sum_phrases(session, phrase_param):
    open_none = get_total(session, params={'limit': 0})
    open_true = get_total(session, params={phrase_param: True, 'limit': 0})
    open_false = get_total(session, params={phrase_param: False, 'limit': 0})
    assert open_false + open_true == open_none
