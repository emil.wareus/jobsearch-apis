import pytest

from tests.test_resources.test_settings import DAWN_OF_TIME, NUMBER_OF_ADS, current_time_stamp
from tests.test_resources.helper import get_search, compare
from common.settings import POSITION, POSITION_RADIUS

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.parametrize('params, expected_number_of_hits', [
    ({'published-before': '2021-04-25T00:00:01'}, NUMBER_OF_ADS),
    ({'published-before': current_time_stamp}, NUMBER_OF_ADS),
    ({'published-before': DAWN_OF_TIME}, 0),
    ({'published-before': '2021-01-01T00:00:01'}, 158),
    ({'published-before': '2021-01-25T07:29:41'}, 304),
    ({'published-after': '2020-11-01T00:00:01'}, 5007),
    ({'published-after': '2020-12-01T00:00:01'}, 4964),
    ({'published-after': '2021-03-10T00:00:01'}, 2810),
    ({'published-after': '2021-03-22T00:00:01'}, 892),
    ({'published-after': DAWN_OF_TIME}, NUMBER_OF_ADS),
    ({'published-after': current_time_stamp}, 0),
    ({'published-after': '2020-12-15T00:00:01', 'published-before': '2020-12-20T00:00:01'}, 20),
    ({'published-after': '2020-12-01T00:00:01', 'published-before': '2020-12-10T00:00:01'}, 26),
    ({'published-after': '2020-12-11T00:00:01', 'published-before': '2020-12-15T00:00:01'}, 8),
    ({'published-after': current_time_stamp, 'published-before': DAWN_OF_TIME}, 0),
])
def test_query_params_date(session, params, expected_number_of_hits):
    """
    Test 'published-before' and 'published-after' query parameters
    With a narrower time span, lower number of hits are returned
    """
    result = get_search(session, params)
    compare(result['total']['value'], expected_number_of_hits)


@pytest.mark.parametrize('params, expected_number_of_hits', [
    ({'parttime.min': '50'}, 4113),
    ({'parttime.min': '80'}, 3914),
    ({'parttime.min': '20'}, 4208),
    ({'parttime.max': '80'}, 103),
    ({'parttime.max': '50'}, 27),
    ({'parttime.max': '20'}, 4)
])
def test_query_params_part_time(session, params, expected_number_of_hits):
    """
    Test 'parttime.min' and 'parttime.max' query parameters
    """
    result = get_search(session, params)
    compare(result['total']['value'], expected_number_of_hits)


@pytest.mark.parametrize('params, expected_number_of_hits', [
    ({POSITION: '59.3,18.0'}, 65),  # stockholm
    ({POSITION: '59.3,18.0', POSITION_RADIUS: 6}, 718),
    ({POSITION: '59.3,18.0', POSITION_RADIUS: 10}, 875),
    ({POSITION: '59.3,18.0', POSITION_RADIUS: 50}, 1176),
    ({POSITION: '59.3,18.0', POSITION_RADIUS: 100}, 1500),
    ({POSITION: '56.9,12.5', POSITION_RADIUS: 100}, 822),
    ({POSITION: '56.9,12.5', POSITION_RADIUS: 50}, 104),
    ({POSITION: '56.9,12.5', POSITION_RADIUS: 10}, 21),
    ({POSITION: '18.0,59.3'}, 0)  # lat long reversed
])
def test_query_params_geo_position(session, params, expected_number_of_hits):
    """
    Test 'position' query parameter along with 'position-radius'
    With larger radius, more hits are returned
    """
    result = get_search(session, params)
    compare(result['total']['value'], expected_number_of_hits)


@pytest.mark.parametrize('params, expected_number_of_hits',
                         [
                             ({'employer': 'västra götalandsregionen'}, 56),
                             ({'employer': 'Jobtech'}, 0),
                             ({'employer': 'Region Stockholm'}, 372),
                             # Todo: this is way too much
                             ({'employer': 'City Gross Sverige AB'}, 3623),
                             ({'employer': 'City Dental i Stockholm AB'}, 3681),
                             ({'employer': 'Premier Service Sverige AB'}, 3633),
                             ({'employer': 'Smartbear Sweden AB'}, 3613),
                             # probably too much:
                             ({'employer': 'Malmö Universitet'}, 109),
                             ({'employer': 'Göteborgs Universitet'}, 89),
                             ({'employer': 'Blekinge Läns Landsting'}, 13),
                             ({'employer': 'Skåne Läns Landsting'}, 72),
                             ({'employer': '"Skåne Läns Landsting"'}, 72),  # quoted string for employer

                         ])
def test_query_params_employer(session, params, expected_number_of_hits):
    """
    This test return too many hits
    it will return hits where company name has one of the words in the employer name (e.g. 'Sverige')
    keeping it to document current behavior
    """
    result = get_search(session, params)
    compare(result['total']['value'], expected_number_of_hits)
