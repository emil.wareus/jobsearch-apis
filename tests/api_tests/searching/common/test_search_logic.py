import pytest

from tests.test_resources.concept_ids import concept_ids_geo as geo
from tests.test_resources.helper import get_search, get_search_check_number_of_results, check_freetext_concepts, \
    compare_multiple
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.integration
@pytest.mark.parametrize("query, municipality, code, municipality_concept_id, expected_number_of_hits", [
    ('bagare stockholm', 'Stockholm', '0180', geo.stockholm, 0),
    ('lärare stockholm', 'Stockholm', '0180', geo.stockholm, 14),
    ('lärare göteborg', 'Göteborg', '1480', geo.goteborg, 4),
])
def test_freetext_work_and_location_details(session, query, municipality, code, municipality_concept_id,
                                            expected_number_of_hits):
    params = {'q': query, 'limit': '100'}
    list_of_ads = get_search_check_number_of_results(session, expected_number_of_hits, params)
    compare_these = []
    for ad in list_of_ads:
        compare_these.extend([
            (ad['workplace_address']['municipality'], municipality, 'Wrong municipality'),
            (ad['workplace_address']['municipality_code'], code, 'Wrong code'),
            (ad['workplace_address']['municipality_concept_id'], municipality_concept_id, 'Wrong concept id'),
        ])
    compare_multiple(compare_these)


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("query, expected_ids_and_relevance", [
    ('sjuksköterska läkare Stockholm Göteborg', [
        ('24645625', 1.0),
        ('24648363', 0.760538553296673),
        ('24648149', 0.760538553296673),
        ('24645837', 0.760538553296673),
        ('24643198', 0.760538553296673),
        ('24642963', 0.760538553296673),
        ('24636423', 0.760538553296673),
        ('24631812', 0.760538553296673),
        ('24627762', 0.760538553296673),
        ('24624501', 0.760538553296673)
    ])])
def test_freetext_two_work_and_two_locations_check_order(session, query, expected_ids_and_relevance):
    """
    Tests that the sorting order of hits is as expected and that relevance value has not changed
    This documents current behavior
    """
    params = {'q': query, 'limit': '10'}
    response_json = get_search(session, params)
    relevance = 0
    old_relevance = 1
    for index, hit in enumerate(response_json['hits']):
        relevance = hit['relevance']
        print(f"('{hit['id']}', {relevance}),")
    for index, hit in enumerate(response_json['hits']):
        assert old_relevance >= relevance  # check that results are presented in ascending relevance order
        assert hit['id'] == expected_ids_and_relevance[index][0]
        assert hit['relevance'] == expected_ids_and_relevance[index][1], hit['id']
        old_relevance = relevance


@pytest.mark.parametrize("query, top_id, expected_number_of_hits", [
    ('bagare kock Stockholm Göteborg', '24599773', 4),
    ('kock bagare Stockholm Göteborg', '24599773', 4),
    ('kallskänka kock Stockholm Göteborg', '24599773', 4),
    ('lärare lågstadielärare Malmö Göteborg', '24650545', 14),
])
def test_freetext_two_work_and_two_locations(session, query, top_id, expected_number_of_hits):
    """
    Test that the top hit for a search has not changed and that the number of hits for query has not changed
    This documents current behavior
    """
    params = {'q': query, 'limit': '100'}
    list_of_ads = get_search_check_number_of_results(session, expected_number_of_hits, params)
    if TEST_USE_STATIC_DATA:
        assert list_of_ads[0]['id'] == top_id


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.integration
@pytest.mark.parametrize("query, expected_number, top_id", [
    ('Bauhaus Kundtjänst', 94, '24583746'),
    ('Sirius crew', 4, '24625524'),
    ('super', 7, '24480758'),
    ('Säsong', 20, '24570725'),
])
def test_freetext_search(session, query, expected_number, top_id):
    """
    Tests from examples
    Test that specific queries should return only one hit (identified by id)
    and that freetext concepts are not included in search result
    """
    params = {'q': query, 'limit': '40'}
    response_json = get_search(session, params=params)
    # freetext concepts should be empty
    assert response_json['total']['value'] == expected_number
    check_freetext_concepts(response_json['freetext_concepts'], [[], [], [], [], [], [], [], [], []])
    if TEST_USE_STATIC_DATA:
        assert response_json['hits'][0]['id'] == top_id


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
def test_search_rules(session):
    params = {'q': "systemutvecklare python java stockholm 'klarna bank ab'", 'limit': '1'}
    response_json = get_search(session, params=params)
    hit = response_json['hits'][0]
    check_freetext_concepts(response_json['freetext_concepts'],
                            [['python', 'java', 'bank'], ['systemutvecklare'], ['stockholm'], [], [], [], [], [], []])
    assert 'klarna' in hit['employer']['name'].lower()
    assert 'klarna' in hit['employer']['workplace'].lower()
    assert 'Systemutvecklare/Programmerare' in hit['occupation']['label']
    assert hit['workplace_address']['municipality'] == 'Stockholm'
