import pytest
from tests.test_resources.helper import get_search, compare
from tests.test_resources.test_settings import EXPECTED_GYMNASIE_LARARE

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.smoke
@pytest.mark.parametrize("query, expected_number_of_hits, identifier", [
    ('"gymnasielärare"', 18, 'a'),
    ("'gymnasielärare'", EXPECTED_GYMNASIE_LARARE, 'b'),
    ("\"gymnasielärare\"", 18, 'c'),
    ("\'gymnasielärare\'", EXPECTED_GYMNASIE_LARARE, 'd'),
    ("""gymnasielärare""", EXPECTED_GYMNASIE_LARARE, 'e'),
    ('''gymnasielärare''', EXPECTED_GYMNASIE_LARARE, 'f'),
    ('gymnasielärare', EXPECTED_GYMNASIE_LARARE, 'g'),
    ("gymnasielärare""", EXPECTED_GYMNASIE_LARARE, 'h'),
    ("gymnasielärare\"", EXPECTED_GYMNASIE_LARARE, 'i'),
    ("gymnasielärare\'", EXPECTED_GYMNASIE_LARARE, 'j'),
    (r"""gymnasielärare""", EXPECTED_GYMNASIE_LARARE, 'k'),
    (r'''gymnasielärare''', EXPECTED_GYMNASIE_LARARE, 'l'),
    ("gymnasielärare lärare", 259, 'm'),
    ("""'gymnasielärare'""", EXPECTED_GYMNASIE_LARARE, 'n'),
    ('''"gymnasielärare" "lärare"''', 357, 'o'),
    ('''"gymnasielärare lärare"''', 0, 'p'),
    ("\"gymnasielärare\"", 18, 'q'),
    ("\"gymnasielärare", 18, 'r'),
    ('''"gymnasielärare"''', 18, 's'),
    ("gymnasielärare", EXPECTED_GYMNASIE_LARARE, 't'),
    ('gymnasielärare', EXPECTED_GYMNASIE_LARARE, 'u'),
])
def test_query_with_different_quotes(session, query, expected_number_of_hits, identifier):
    json_response = get_search(session, params={'q': query, 'limit': '0'})
    compare(json_response['total']['value'], expected_number_of_hits, msg=f'Query: {query}')


@pytest.mark.parametrize('query, expected', [
    ('"c++', 62),
    ('"c++"', 62),
    ('"c+', 41),
    ('"c( ', 40),
])
def test_cplusplus_in_quotes(session, query, expected):
    """
    Test for a bug where some quotes caused an 'internal server error' response
    """
    param = {'q': query, 'limit': 0}
    assert get_search(session, param)['total']['value'] == expected


@pytest.mark.parametrize("query, expected_number_of_hits", [
    ('python stockholm', 27),
    ('"python stockholm"', 0),
    ('"python" "stockholm"', 957),
    ('"python" stockholm', 843),
    ('python "stockholm"', 76),
    ('"python job in stockholm"', 0),
    ('"work from home" python stockholm', 27)
])
def test_query_with_quotes(session, query, expected_number_of_hits):
    json_response = get_search(session, params={'q': query, 'limit': '100'})
    compare(json_response['total']['value'], expected_number_of_hits, msg=f'Query: {query}')
