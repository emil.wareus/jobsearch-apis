import pytest

from tests.test_resources.helper import get_search
from tests.test_resources.test_settings import TEST_USE_STATIC_DATA

# marks all tests as jobsearch and historical
pytestmark = [pytest.mark.jobsearch, pytest.mark.historical]


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("query, expected", [
    ('försäljning/marknad', 3),
    ('försäljning marknad', 12),
    ('försäljning / marknad', 28),
    ('lager/logistik', 19),
    ('lager / logistik', 7),
    ('lager logistik', 137),
    ('psykolog/beteendevetare', 20),
    ('psykolog / beteendevetare', 0),
    ('psykolog beteendevetare', 30),
    ('Affärsutvecklare/exploateringsingenjör', 3),
    ('Affärsutvecklare / exploateringsingenjör', 1),
    ('Affärsutvecklare exploateringsingenjör', 11),
    ('Affärsutvecklare/exploateringsingenjörer', 2),
    ('Affärsutvecklare / exploateringsingenjörer', 1),
    ('Affärsutvecklare exploateringsingenjörer', 11),
    ('barnpsykiatri/habilitering', 1),
    ('barnpsykiatri / habilitering', 0),
    ('barnpsykiatri habilitering', 21),
    ('mentor/kontaktlärare', 0),
    ('mentor / kontaktlärare', 0),
    ('mentor kontaktlärare', 0),
    ('Verktygsmakare/Montör', 17),
    ('Verktygsmakare / Montör', 4),
    ('Verktygsmakare Montör', 38),
    ('Kolloledare/specialpedagog', 18),
    ('Kolloledare / specialpedagog', 0),
    ('Kolloledare specialpedagog', 24),
    ('fritidshem/fritidspedagog', 15),
    ('fritidshem / fritidspedagog', 3),
    ('fritidshem fritidspedagog', 17),
    ('UX/UI Designer', 1),
    ('UX / UI Designer', 1),
    ('UX UI Designer', 1),
])
def test_freetext_search_slash(session, query, expected):
    """
    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    """
    params = {'q': query, 'limit': '0'}
    assert get_search(session, params=params)['total']['value'] == expected


@pytest.mark.skipif(not TEST_USE_STATIC_DATA, reason="depends on a fixed set of ads")
@pytest.mark.parametrize("query, expected", [
    ('.NET/C#', 13),
    ('.NET / C#', 7),
    ('.NET C#', 63),
    ('.NET /C#', 2),
    ('.NET/ C#', 9),
    ('.NET', 38),
    ('C#/.net', 12),
    ('C# .net', 63),
    ('C# /.net', 9),
    ('C# / .net', 7),
    ('C#', 47),
    ('C#/.net', 12),
    ('C# .net', 63),
    ('C# /.net', 9),
    ('dotnet', 37)
])
def test_freetext_search_dot_hash_slash(session, query, expected):
    """
    Search with terms that are joined by a slash '/' included (x/y)
    with the terms separately (x y)
    and with a slash surrounded by space (x / y)
    for words that have . or # (e.g. '.net', 'c#')
    """
    params = {'q': query, 'limit': '0'}
    assert get_search(session, params=params)['total']['value'] == expected


@pytest.mark.smoke
@pytest.mark.parametrize("query, expected", [
    ('programmerare', 165),
    ('Systemutvecklare', 165),
    ('Systemutvecklare/Programmerare', 14),
    ('Systemutvecklare Programmerare', 165),
    ('Systemutvecklare / Programmerare', 22)
])
def test_freetext_search_slash_short(session, query, expected):
    params = {'q': query, 'limit': '0'}
    assert get_search(session, params=params)['total']['value'] == expected
