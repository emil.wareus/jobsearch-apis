import pytest

from common.settings import OCCUPATION_CONCEPT_ID, LOCATION_CONCEPT_ID, UPDATED_BEFORE_DATE
from tests.test_resources.test_settings import NUMBER_OF_ADS, DAWN_OF_TIME, current_time_stamp

import tests.test_resources.concept_ids.concept_ids_geo as geo
import tests.test_resources.concept_ids.occupation as work
import tests.test_resources.concept_ids.occupation_group as group
import tests.test_resources.concept_ids.occupation_field as field
from tests.test_resources.helper import get_stream_check_number_of_results

# mark all tests in this file as @pytest.mark.jobstream
pytestmark = pytest.mark.jobstream


@pytest.mark.parametrize('date, work, expected_number_of_hits', [
    (DAWN_OF_TIME, group.arbetsformedlare, 22),
    (DAWN_OF_TIME, group.apotekare, 8),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, 233),
    (DAWN_OF_TIME, work.mjukvaruutvecklare, 50),
    (DAWN_OF_TIME, work.arbetsterapeut, 28)])
def test_filter_only_on_occupation(session, date, work, expected_number_of_hits):
    """
    Returns number of hits in the db. Temporary to verify results in other tests
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, geo, expected_number_of_hits', [
    (DAWN_OF_TIME, geo.aland_tillhor_finland, 0),
    (DAWN_OF_TIME, geo.norge, 10),
    (DAWN_OF_TIME, geo.malta, 1),
    (DAWN_OF_TIME, geo.schweiz, 1),
    (DAWN_OF_TIME, geo.kalmar_lan, 140),
    (DAWN_OF_TIME, geo.botkyrka, 20),
    (DAWN_OF_TIME, geo.stockholms_lan, 1227),
    (DAWN_OF_TIME, geo.stockholm, 775),
    (DAWN_OF_TIME, geo.sverige, 4999)])
def test_filter_only_on_location(session, date, geo, expected_number_of_hits):
    """
    Returns number of hits in the db. Temporary to verify results in other tests
    """
    params = {'date': date, LOCATION_CONCEPT_ID: geo}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, work, geo, expected_number_of_hits', [
    (DAWN_OF_TIME, work.larare_i_fritidshem_fritidspedagog, geo.vastra_gotalands_lan, 5),
    ('2020-12-15T00:00:01', work.larare_i_fritidshem_fritidspedagog, geo.vastra_gotalands_lan, 5),
    ('2020-12-01T00:00:01', work.servitor_servitris__kafe_och_konditori, geo.sverige, 2),
    ('2020-12-01T00:00:01', work.bussforare_busschauffor, geo.sverige, 3),
    ('2020-12-01T00:00:01', work.larare_i_grundskolan__arskurs_7_9, geo.stockholms_lan, 10),
    (DAWN_OF_TIME, group.grundutbildade_sjukskoterskor, geo.sverige, 393),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, geo.sverige, 231),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, geo.schweiz, 0),
    ('2020-11-01T00:00:01', group.mjukvaru__och_systemutvecklare_m_fl_, geo.sverige, 230),
    ('2020-12-01T00:00:01', field.militart_arbete, geo.schweiz, 0),
    (DAWN_OF_TIME, field.militart_arbete, geo.sverige, 5),
    (DAWN_OF_TIME, field.halso__och_sjukvard, geo.sverige, 965),
    (DAWN_OF_TIME, field.halso__och_sjukvard, geo.stockholms_lan, 195),
    ('2020-11-25T00:00:01', field.halso__och_sjukvard, geo.sverige, 958),
    ('2020-12-15T00:00:01', field.halso__och_sjukvard, geo.stockholms_lan, 192),
])
def test_filter_with_date_and_occupation_and_location(session, date, work, geo,
                                                      expected_number_of_hits):
    """
    should return results based on date AND occupation type AND location
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work, LOCATION_CONCEPT_ID: geo}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, concept_id, expected_number_of_hits', [
    (DAWN_OF_TIME, work.personlig_assistent, 181),
    ('2020-11-01T00:00:01', work.personlig_assistent, 181),
    ('2020-12-15T00:00:01', work.personlig_assistent, 179),
    ('2021-01-01T00:00:01', work.personlig_assistent, 177),
    ('2021-03-01T00:00:01', work.personlig_assistent, 122),
    (DAWN_OF_TIME, work.kassapersonal, 7),
    ('2020-12-01T00:00:01', work.kassapersonal, 7),
    (DAWN_OF_TIME, work.mjukvaruutvecklare, 50),
    ('2020-10-25T00:00:00', work.mjukvaruutvecklare, 49),
    ('2020-11-25T00:00:00', work.mjukvaruutvecklare, 47),
    ('2020-12-15T00:00:00', work.mjukvaruutvecklare, 46),
    (DAWN_OF_TIME, group.arbetsformedlare, 22),
    ('2021-02-25T00:00:00', group.arbetsformedlare, 19),
    (DAWN_OF_TIME, field.militart_arbete, 5),
    (DAWN_OF_TIME, field.socialt_arbete, 346),
    (DAWN_OF_TIME, work.administrativ_chef, 4),
    (DAWN_OF_TIME, work.account_manager, 42),
    (DAWN_OF_TIME, work.cykelbud, 2),
])
def test_filter_with_date_and_one_occupation(session, date, concept_id, expected_number_of_hits):
    """
    test of filtering in /stream: should return results based on date AND occupation-related concept_id
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: concept_id}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, geo, expected_number_of_hits', [
    (DAWN_OF_TIME, geo.falun, 33),
    (DAWN_OF_TIME, geo.stockholms_lan, 1227),
    ('2020-12-01T00:00:01', geo.stockholms_lan, 1211),
    ('2021-03-05T00:00:01', geo.stockholms_lan, 832),
    (DAWN_OF_TIME, geo.norge, 10),
    ('2021-01-01T00:00:01', geo.hallands_lan, 136),
    ('2021-01-01T00:00:01', geo.linkoping, 103),
    (DAWN_OF_TIME, geo.sverige, 4999),
    ('2020-11-01T00:00:01', geo.sverige, 4981),
    ('2020-12-15T00:00:01', geo.sverige, 4912),
    ('2020-12-01T00:00:01', geo.stockholm, 762),
    ('2020-12-01T00:00:01', geo.schweiz, 1),
    ('2021-02-01T00:00:01', geo.norge, 8),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, 121),
    ('2020-12-01T00:00:01', geo.dalarnas_lan, 118),
    ('2021-02-15T00:00:01', geo.dalarnas_lan, 99)])
def test_filter_with_date_and_location(session, date, geo, expected_number_of_hits):
    """
    should return results based on date AND occupation type AND location_1
    """
    params = {'date': date, LOCATION_CONCEPT_ID: geo}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


#   multiple params of same type

@pytest.mark.parametrize('date, work_1, work_2, expected_number_of_hits', [
    (DAWN_OF_TIME, work.account_manager, work.cykelbud, 44),
    (DAWN_OF_TIME, work.mjukvaruutvecklare, group.arbetsformedlare, 72),
    (DAWN_OF_TIME, work.cykelbud, work.account_manager, 44),
    ('2020-11-01T00:00:01', work.mjukvaruutvecklare, group.arbetsformedlare, 71),
    ('2020-12-01T00:00:01', work.mjukvaruutvecklare, group.arbetsformedlare, 69),
    (DAWN_OF_TIME, work.administrativ_chef, field.militart_arbete, 9),
    (DAWN_OF_TIME, field.militart_arbete, work.administrativ_chef, 9),
    ('2020-11-01T00:00:01', group.bagare_och_konditorer, group.bartendrar, 6),
    (DAWN_OF_TIME, group.bagare_och_konditorer, group.bartendrar, 6),
    ('2020-11-11T00:00:01', group.apotekare, field.data_it, 422),
    ('2020-12-12T00:00:01', group.apotekare, field.data_it, 412),
    ('2021-01-25T00:00:00', group.frisorer, work.akupunktor, 11),
    ('2020-11-25T00:00:00', field.pedagogiskt_arbete, field.halso__och_sjukvard, 1490),
    ('2020-12-15T00:00:00', field.hantverksyrken, group.hudterapeuter, 21),
    ('2020-11-25T00:00:00', field.socialt_arbete, work.databasutvecklare, 348)])
def test_filter_with_date_and_two_occupations(session, date, work_1, work_2,
                                              expected_number_of_hits):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on both date AND (work_1 OR work_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2]}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, work_1, work_2, work_3, expected_number_of_hits', [
    (DAWN_OF_TIME, work.account_manager, work.cykelbud, work.databasutvecklare, 46),
    ('2020-11-01T00:00:01', work.mjukvaruutvecklare, work.databasutvecklare, group.arbetsformedlare, 73),
    ('2020-12-01T00:00:01', work.administrativ_chef, work.apotekschef, field.militart_arbete, 10),
    ('2020-11-01T00:00:01', group.bagare_och_konditorer, group.bartendrar, group.hovmastare_och_servitorer, 36),
    ('2020-12-01T00:00:01', group.apotekare, group.ambulanssjukskoterskor_m_fl_, field.data_it, 425),
    ('2020-11-25T00:00:00', group.frisorer, group.hudterapeuter, work.akupunktor, 16),
    (DAWN_OF_TIME, field.pedagogiskt_arbete, field.halso__och_sjukvard, field.kropps__och_skonhetsvard, 1524),
    ('2020-11-25T00:00:00', field.pedagogiskt_arbete, field.halso__och_sjukvard, field.kropps__och_skonhetsvard, 1514),
    (DAWN_OF_TIME, field.hantverksyrken, field.data_it, group.hudterapeuter, 443),
    ('2020-11-25T00:00:00', field.hantverksyrken, field.data_it, group.hudterapeuter, 429),
    (DAWN_OF_TIME, field.socialt_arbete, field.bygg_och_anlaggning, work.databasutvecklare, 539),
    ('2020-11-25T00:00:00', field.socialt_arbete, field.bygg_och_anlaggning, work.databasutvecklare, 537)
])
def test_filter_with_date_and_three_occupations(session, date, work_1, work_2, work_3,
                                                expected_number_of_hits):
    """
    test of filtering in /stream with date and 2 occupation-related concept_ids
    should return results based on date AND (work_1 OR work_2 OR work 3)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2, work_3]}
    get_stream_check_number_of_results(session, expected_number_of_hits, params=params)


@pytest.mark.smoke
@pytest.mark.parametrize('date, work_1, work_2, geo_1, expected_number_of_hits', [
    (DAWN_OF_TIME, work.arbetsterapeut, work.bussforare_busschauffor, geo.sverige, 31),
    (DAWN_OF_TIME, work.arbetsterapeut, group.apotekare, geo.vastra_gotalands_lan, 7),
    (DAWN_OF_TIME, work.arbetsterapeut, field.kropps__och_skonhetsvard, geo.norge, 0),
    (DAWN_OF_TIME, work.arbetsterapeut, field.kropps__och_skonhetsvard, geo.sverige, 53),
    (DAWN_OF_TIME, group.grundutbildade_sjukskoterskor, group.grundutbildade_sjukskoterskor, geo.sverige, 393),
    (DAWN_OF_TIME, group.apotekare, group.hovmastare_och_servitorer, geo.stockholms_lan, 5),
    (DAWN_OF_TIME, group.apotekare, group.arbetsformedlare, geo.sverige, 30),
    ('2020-12-01T00:00:01', field.militart_arbete, field.hantverksyrken, geo.stockholm, 4),
    ('2020-12-01T00:00:01', field.militart_arbete, group.ambulanssjukskoterskor_m_fl_, geo.sverige, 15),
    ('2020-12-01T00:00:01', field.militart_arbete, work.bussforare_busschauffor, geo.norge, 0)])
def test_filter_with_date_and_two_occupations_and_location(session, date, work_1, work_2, geo_1,
                                                           expected_number_of_hits):
    """
    should return results based on date AND location AND (work_1 OR work_2)
    results = work_1 + work_2 that matches location
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2], LOCATION_CONCEPT_ID: geo_1}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, geo_1, geo_2, expected_number_of_hits', [
    (DAWN_OF_TIME, geo.arboga, geo.falun, 39),
    (DAWN_OF_TIME, geo.arboga, geo.stockholms_lan, 1233),
    (DAWN_OF_TIME, geo.arboga, geo.norge, 16),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, geo.hallands_lan, 258),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, geo.linkoping, 230),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, geo.sverige, 4981),
    ('2020-12-01T00:00:01', geo.schweiz, geo.stockholm, 763),
    ('2020-12-01T00:00:01', geo.schweiz, geo.jonkopings_lan, 195),
    ('2020-12-01T00:00:01', geo.schweiz, geo.norge, 11),
    ('2020-11-01T00:00:01', geo.dalarnas_lan, geo.schweiz, 122),
    ('2020-12-01T00:00:01', geo.schweiz, geo.norge, 11)])
def test_filter_with_date_and_two_locations(session, date, geo_1, geo_2, expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, LOCATION_CONCEPT_ID: [geo_1, geo_2]}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, geo_list, expected_number_of_hits', [
    (DAWN_OF_TIME, [geo.stockholms_lan], 1227),
    (DAWN_OF_TIME, [geo.stockholms_lan, geo.solna], 1227),
    (DAWN_OF_TIME, [geo.stockholms_lan, geo.stockholm, geo.botkyrka], 1227),
    (DAWN_OF_TIME, [geo.stockholms_lan, geo.stockholm, geo.botkyrka, geo.solna, geo.nacka], 1227)])
def test_filter_with_date_and_multiple_locations_in_same_region(session, date, geo_list,
                                                                expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, LOCATION_CONCEPT_ID: geo_list}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, work_list, expected_number_of_hits', [
    (DAWN_OF_TIME, [field.halso__och_sjukvard], 971),
    (DAWN_OF_TIME, [field.halso__och_sjukvard, work.sjukskoterska__grundutbildad], 971),
    (DAWN_OF_TIME, [field.halso__och_sjukvard, group.grundutbildade_sjukskoterskor], 971),
    (DAWN_OF_TIME,
     [field.halso__och_sjukvard, group.grundutbildade_sjukskoterskor, group.ambulanssjukskoterskor_m_fl_,
      work.sjukskoterska__medicin_och_kirurgi], 971)])
def test_filter_with_date_and_multiple_occupations_within_same_field(session, date, work_list,
                                                                     expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work_list}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, work_list, expected_number_of_hits', [
    (DAWN_OF_TIME, [field.halso__och_sjukvard], 971),
    (DAWN_OF_TIME, [group.grundutbildade_sjukskoterskor], 396),
    (DAWN_OF_TIME, [work.sjukskoterska__grundutbildad], 392)
])
def test_filter_narrowing_down_occupations_within_same_field(session, date, work_list,
                                                             expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work_list}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, work, geo_1, geo_2, expected_number_of_hits', [
    (DAWN_OF_TIME, work.arbetsterapeut, geo.vasteras, geo.falun, 2),
    (DAWN_OF_TIME, work.arbetsterapeut, geo.vastra_gotalands_lan, geo.ostergotlands_lan, 4),
    (DAWN_OF_TIME, group.civilingenjorsyrken_inom_bygg_och_anlaggning, geo.dalarnas_lan, geo.hallands_lan, 0),
    (DAWN_OF_TIME, group.civilingenjorsyrken_inom_bygg_och_anlaggning, geo.dalarnas_lan, geo.linkoping, 0),
    (DAWN_OF_TIME, group.civilingenjorsyrken_inom_bygg_och_anlaggning, geo.malta, geo.sverige, 38),
    ('2020-12-01T00:00:01', field.kultur__media__design, geo.schweiz, geo.stockholm, 11),
    ('2020-12-01T00:00:01', field.naturvetenskapligt_arbete, geo.schweiz, geo.stockholms_lan, 13),
    ('2020-12-01T00:00:01', field.bygg_och_anlaggning, geo.schweiz, geo.norge, 1),
    (DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, geo.vastra_gotalands_lan, geo.schweiz, 61),
    ('2020-02-01T00:00:01', work.bussforare_busschauffor, geo.schweiz, geo.norge, 0)])
def test_filter_with_date_and_occupation_and_two_locations(session, date, work, geo_1, geo_2,
                                                           expected_number_of_hits):
    """
    should return results based on date AND occupation type AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: work, LOCATION_CONCEPT_ID: [geo_1, geo_2]}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


@pytest.mark.parametrize('date, work_1, work_2, geo_1, geo_2, expected_number_of_hits', [
    (DAWN_OF_TIME, work.databasutvecklare, work.arbetsterapeut, geo.goteborg, geo.falun, 2),
    (DAWN_OF_TIME, work.databasutvecklare, work.sjukskoterska__grundutbildad, geo.arboga, geo.falun, 3),
    (
            DAWN_OF_TIME, group.mjukvaru__och_systemutvecklare_m_fl_, work.arbetsterapeut, geo.arboga,
            geo.stockholms_lan, 109),
    (DAWN_OF_TIME, work.farmaceut_apotekare, group.kassapersonal_m_fl_, geo.dalarnas_lan, geo.hallands_lan, 1),
    (DAWN_OF_TIME, work.sjukskoterska__grundutbildad, group.apotekare, geo.dalarnas_lan, geo.linkoping, 18),
    (DAWN_OF_TIME, work.barnsjukskoterska, group.apotekare, geo.malta, geo.sverige, 17),
    ('2020-12-01T00:00:01', work.eltekniker, field.kultur__media__design, geo.schweiz, geo.stockholm, 11),
    ('2020-12-01T00:00:01', work.butikssaljare__fackhandel, field.naturvetenskapligt_arbete, geo.schweiz,
     geo.stockholms_lan, 23),
    ('2020-12-01T00:00:01', group.gymnasielarare, field.bygg_och_anlaggning, geo.schweiz, geo.norge, 1),
    ('2020-12-01T00:00:01', group.grundutbildade_sjukskoterskor, field.bygg_och_anlaggning, geo.schweiz, geo.norge, 4),
    ('2020-12-01T00:00:01', group.grundutbildade_sjukskoterskor, field.halso__och_sjukvard, geo.schweiz, geo.norge, 5),
    (DAWN_OF_TIME, work.bygg__och_anlaggningsforare, group.mjukvaru__och_systemutvecklare_m_fl_, geo.dalarnas_lan,
     geo.schweiz, 1),
    ('2020-12-01T00:00:01', field.halso__och_sjukvard, work.bussforare_busschauffor, geo.schweiz, geo.norge, 5)])
def test_filter_with_date_and_two_occupations_and_two_locations(session, date, work_1, work_2, geo_1, geo_2,
                                                                expected_number_of_hits):
    """
    should return results based on date AND (occupation 1 OR occupation 2) AND (location_1 OR location_2)
    """
    params = {'date': date, OCCUPATION_CONCEPT_ID: [work_1, work_2], LOCATION_CONCEPT_ID: [geo_1, geo_2]}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)


# test below for comparison of number of hits for different dates
@pytest.mark.parametrize('date, expected_number_of_hits', [
    (DAWN_OF_TIME, NUMBER_OF_ADS),
    ('2020-12-01T00:00:01', 4975),
    ('2021-01-01T00:00:01', 4904),
    ('2021-01-25T00:00:00', 4788),
    ('2021-02-01T00:00:00', 4717),
    ('2021-02-05T00:00:00', 4650),
    ('2021-02-10T00:00:00', 4591),
    ('2021-02-15T00:00:00', 4505),
    ('2021-03-01T00:00:00', 3963),
    ('2021-03-10T00:00:00', 2973),
    ('2021-03-15T00:00:00', 2276),
    ('2021-03-20T12:00:00', 1022),
    ('2021-03-22T12:00:00', 844),
    ('2021-03-23T12:00:00', 542),
    ('2021-03-24T12:00:00', 192),
    ('2021-03-25T12:30:40', 6),
    ('2021-03-30T00:00:00', 6),
])
def test_filter_only_on_date(session, date, expected_number_of_hits):
    """
    Test basic stream with filtering on date (update after this date)
    """
    get_stream_check_number_of_results(session, expected_number_of_hits, params={'date': date})


@pytest.mark.parametrize('from_date, to_date, expected_number_of_hits', [
    # 1 verify that results are the same as when only using a single date
    (DAWN_OF_TIME, current_time_stamp, NUMBER_OF_ADS),
    ('2020-10-01T00:00:01', '2021-04-30T00:00:00', 5028),
    ('2020-11-01T00:00:01', '2021-04-30T00:00:00', 5011),
    ('2020-11-15T00:00:00', '2021-04-30T00:00:00', 4997),
    ('2020-11-20T00:00:00', '2021-04-30T00:00:00', 4992),
    ('2020-11-30T00:00:00', '2021-04-30T00:00:00', 4976),
    ('2020-12-05T00:00:00', '2021-04-30T00:00:00', 4964),
    ('2020-12-10T00:00:00', '2021-04-30T00:00:00', 4956),
    ('2020-12-15T00:00:00', '2021-04-30T00:00:00', 4942),
    ('2020-12-20T00:00:00', '2021-04-30T00:00:00', 4929),
    ('2021-03-30T08:29:41', '2021-04-30T00:00:00', 6),
    ('2020-11-25T00:00:00', '2021-11-25T00:00:00', 4987),
    ('2020-12-14T00:00:00', '2021-03-30T00:00:00', 4941),
    ('2020-12-14T00:00:00', '2020-12-16T00:00:00', 6),
    ('2020-11-14T00:00:00', '2020-11-20T00:00:00', 5),
    ('2020-11-25T00:00:00', '2020-11-30T00:00:00', 11),
    ('2020-11-26T00:00:00', '2020-11-30T00:00:00', 6),
    ('2020-12-10T00:00:00', '2020-12-15T00:00:00', 14),
    ('2020-12-12T00:00:00', '2020-12-15T00:00:00', 6),
    ('2020-12-15T00:00:00', '2020-12-16T00:00:00', 1),
    ('2020-12-16T00:00:00', '2020-12-17T10:00:00', 5),
    ('2020-12-22T00:00:00', '2020-12-23T10:00:00', 4),
    (DAWN_OF_TIME, '2021-04-30T10:00:00', NUMBER_OF_ADS),
    (DAWN_OF_TIME, current_time_stamp, NUMBER_OF_ADS),
    # reverse order should return 0 results without errors
    (current_time_stamp, DAWN_OF_TIME, 0)
])
def test_filter_on_date_interval(session, from_date, to_date, expected_number_of_hits):
    """
    Test stream with filtering on date interval.
    """
    params = {'date': from_date, UPDATED_BEFORE_DATE: to_date}
    get_stream_check_number_of_results(session, expected_number_of_hits, params)
