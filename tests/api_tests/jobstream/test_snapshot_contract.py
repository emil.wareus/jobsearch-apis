# coding=utf-8
import tests.test_resources.ad_fields_data_type as type_check

"""
ads are loaded once in fixture 'snapshot' and reused in all tests using that fixture to save time
"""


def test_types_top_level(snapshot):
    for ad in snapshot:
        type_check.check_types_top_level(ad)


def test_types_occupation(snapshot):
    for ad in snapshot:
        type_check.check_types_occupation(ad)


def test_duration(snapshot):
    for ad in snapshot:
        type_check.check_duration(ad)


def test_employment_type(snapshot):
    for ad in snapshot:
        type_check.check_employment_type(ad)


def test_salary_type(snapshot):
    for ad in snapshot:
        type_check.check_salary_type(ad)


def test_types_employer(snapshot):
    for ad in snapshot:
        type_check.check_types_employer(ad)


def test_scope_of_work(snapshot):
    for ad in snapshot:
        type_check.check_scope_of_work(ad)


def test_application_details(snapshot):
    for ad in snapshot:
        type_check.check_application_details(ad)


def test_job_ad_description(snapshot):
    for ad in snapshot:
        type_check.check_job_ad_description(ad)


def test_workplace_address(snapshot):
    for ad in snapshot:
        type_check.check_workplace_address(ad)


def test_requirements(snapshot):
    for ad in snapshot:
        type_check.check_requirements(ad)


def test_working_hours_type(snapshot):
    for ad in snapshot:
        type_check.check_working_hours_type(ad)


def test_contact_persons(snapshot):
    for ad in snapshot:
        type_check.check_types_contact(ad)


def test_length(snapshot):
    for ad in snapshot:
        type_check.check_length(ad, 34)