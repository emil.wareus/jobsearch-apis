import random
import pytest
import requests

import tests.test_resources.test_settings as settings
from tests.test_resources.helper import get_snapshot_check_number_of_results, get_search, get_stats


@pytest.fixture(scope='session')
def session():
    """
    creates a Session object which will persist over the entire test run ("session").
    http connections will be reused (higher performance, less resource usage)
    Returns a Session object
    """
    s = requests.sessions.Session()
    s.headers.update(settings.test_headers)
    return s


@pytest.fixture(scope='session')
def snapshot(session):
    """
    get snapshot once to save time and resources
    """
    return get_snapshot_check_number_of_results(session, expected_number=None)


@pytest.fixture
def random_ads(session):
    offset = random.randint(0, 1900)
    params = {'limit': 100, 'offset': offset}
    list_of_ads = get_search(session, params)['hits']

    return list_of_ads


@pytest.fixture(scope='session')
def historical_stats(session):
    """
    get stats once to save time and resources
    """
    return get_stats(session, params={})
