import datetime
import pytest
import json
from common import settings, taxonomy
from tests.unit_tests.test_resources.mock_for_querybuilder_tests import all_query_builders, mock_querybuilder_jobsearch
from sokannonser.repository.querybuilder import QueryBuilder
from tests.test_resources.helper import is_dst

# this will run unit tests whenever api or integration tests are run, or when only unit tests are selected for test
from tests.unit_tests.test_resources.qb_helper import _assert_json_structure

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("word, expected", [
    ('"stockholm', '\\"stockholm'),
    ('v]rg]rda', 'v\\]rg\\]rda'),
])
def test_check_search_word_type_nar_862_and_1107(word, expected):
    """
    Checking that special chars are properly escaped for Elastic
    when checking search word type, before suggest_extra_word(),
    for /complete endpoint
    """
    query = json.loads(QueryBuilder.create_check_search_word_type_query(word))
    assert query['aggs']['search_type_location']['terms']['include'] == expected


def test_parse_args_query_with_slash():
    args = {'x-feature-freetext-bool-method': 'and', 'x-feature-disable-smart-freetext': None,
            'x-feature-enable-false-negative': None, 'published-before': None, 'published-after': None,
            'occupation-name': None, 'occupation-group': None, 'occupation-field': None, 'occupation-collection': None,
            'skill': None, 'language': None, 'worktime-extent': None, 'parttime.min': None, 'parttime.max': None,
            'driving-license-required': None, 'driving-license': None, 'employment-type': None, 'experience': None,
            'municipality': None, 'region': None, 'country': None, 'unspecified-sweden-workplace': None, 'abroad': None,
            'position': None, 'position.radius': None, 'employer': None, 'q': 'systemutvecklare/programmerare',
            'qfields': None, 'relevance-threshold': None, 'sort': None, 'stats': None, 'stats.limit': None}

    expected_time = 'now+2H/m' if is_dst() else 'now+1H/m'  # f"{expected_time}"
    expected_query_dsl = \
        {
            'from': 0, 'size': 10, 'track_total_hits': True, 'track_scores': True, 'query': {'bool': {
            'must': [{'bool': {'must': [{'bool': {'should': [{'multi_match': {'query': 'systemutvecklare/programmerare',
                                                                              'type': 'cross_fields', 'operator': 'and',
                                                                              'fields': ['headline^3',
                                                                                         'keywords.extracted.employer^2',
                                                                                         'description.text', 'id',
                                                                                         'external_id', 'source_type',
                                                                                         'keywords.extracted.location^5']}},
                                                             {'match': {'headline.words': {
                                                                 'query': 'systemutvecklare/programmerare',
                                                                 'operator': 'and', 'boost': 5}}}]}}]}}],
            'filter': [{'range': {'publication_date': {'lte': f"{expected_time}"}}},
                       {'range': {'last_publication_date': {'gte': f"{expected_time}"}}},
                       {'term': {'removed': False}}]}},
            'aggs': {'positions': {'sum': {'field': 'number_of_vacancies'}}},
            'sort': ['_score', {'publication_date': 'desc'}]
        }

    assert mock_querybuilder_jobsearch.parse_args(args) == expected_query_dsl


def current_month_and_day():
    now = datetime.datetime.now()
    current_month = now.strftime('%m')
    current_day = now.strftime('%d')
    return current_month, current_day


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("args, exist, expected",
                         [({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 77.7"],
                            settings.POSITION_RADIUS: [5]},
                           True,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "5km",
                                       "workplace_address.coordinates":
                                           [77.7, 66.6]}}]}}),
                          ({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 180.1"],
                            settings.POSITION_RADIUS: [5]},
                           False,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "5km",
                                       "workplace_address.coordinates":
                                           [180.1, 66.6]}}]}}),
                          ({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 77.7"],
                            settings.POSITION_RADIUS: [-5]},
                           False,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "-5km",
                                       "workplace_address.coordinates": [
                                           77.7, 66.6
                                       ]}}]}}),
                          ({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 77.7", "59.1, 18.1"],
                            settings.POSITION_RADIUS: [5, 10]},
                           True,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "5km",
                                       "workplace_address.coordinates": [
                                           77.7, 66.6
                                       ]}},
                                       {"geo_distance": {
                                           "distance": "10km",
                                           "workplace_address.coordinates": [
                                               18.1, 59.1
                                           ]
                                       }}]
                           }}),
                          ({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 77.7", "59.1, 18.1"],
                            settings.POSITION_RADIUS: [5, 10, 15]},
                           True,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "5km",
                                       "workplace_address.coordinates": [
                                           77.7, 66.6
                                       ]}},
                                       {"geo_distance": {
                                           "distance": "10km",
                                           "workplace_address.coordinates": [
                                               18.1, 59.1
                                           ]
                                       }}]
                           }}),
                          ({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 77.7", "59.1, 18.1"],
                            settings.POSITION_RADIUS: [10]},
                           True,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "10km",
                                       "workplace_address.coordinates": [
                                           77.7, 66.6
                                       ]}},
                                       {"geo_distance": {
                                           "distance": "5km",
                                           "workplace_address.coordinates": [
                                               18.1, 59.1
                                           ]
                                       }}]
                           }}),
                          ({settings.APIKEY: "",
                            settings.POSITION: ["66.6, 77.7", "59.1, 18.1"]},
                           True,
                           {"bool": {
                               "should":
                                   [{"geo_distance": {
                                       "distance": "5km",
                                       "workplace_address.coordinates": [
                                           77.7, 66.6
                                       ]}},
                                       {"geo_distance": {
                                           "distance": "5km",
                                           "workplace_address.coordinates": [
                                               18.1, 59.1
                                           ]
                                       }}]
                           }})])
def test_geo_distance_filter(args, exist, expected, mock_query_builder):
    query_dsl = mock_query_builder.parse_args(args)
    assert (expected in query_dsl["query"]["bool"]["filter"]) == exist


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("args, expected_pos, expected_neg",
                         [({settings.APIKEY: "",
                            taxonomy.REGION: ["01", "02"]},
                           [
                               {"term": {"workplace_address.region_code": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.region_code": {"value": "02", "boost": 1.0}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "02", "boost": 1.0}}}
                           ],
                           []),
                          ({settings.APIKEY: "",
                            taxonomy.MUNICIPALITY: ["0111"]},
                           [
                               {"term": {"workplace_address.municipality_code": {"value": "0111", "boost": 2.0}}},
                               {"term": {
                                   "workplace_address.municipality_concept_id": {"value": "0111", "boost": 2.0}}}
                           ],
                           []),
                          ({settings.APIKEY: "",
                            taxonomy.REGION: ["01", "02"],
                            taxonomy.MUNICIPALITY: ["1111", "2222"]},
                           [
                               {"term": {"workplace_address.region_code": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.region_code": {"value": "02", "boost": 1.0}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "02", "boost": 1.0}}},
                               {"term": {"workplace_address.municipality_code": {"value": "1111", "boost": 2.0}}},
                               {"term": {"workplace_address.municipality_code": {"value": "2222", "boost": 2.0}}},
                               {"term": {
                                   "workplace_address.municipality_concept_id":
                                       {"value": "1111", "boost": 2.0}}},
                               {"term": {
                                   "workplace_address.municipality_concept_id":
                                       {"value": "2222", "boost": 2.0}}}
                           ],
                           []),
                          ({settings.APIKEY: "",
                            taxonomy.REGION: ["01", "-02"],
                            taxonomy.MUNICIPALITY: ["1111", "-2222"]},
                           [
                               {"term": {"workplace_address.region_code": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.municipality_code": {"value": "1111", "boost": 2.0}}},
                               {"term": {"workplace_address.region_code": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.municipality_code": {"value": "1111", "boost": 2.0}}}
                           ],
                           [
                               {"term": {"workplace_address.region_code": {"value": "02"}}},
                               {"term": {"workplace_address.municipality_code": {"value": "2222"}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "02"}}},
                               {"term": {
                                   "workplace_address.municipality_concept_id": {"value": "2222"}}}
                           ]),
                          ({settings.APIKEY: "",
                            taxonomy.REGION: ["01", "-02"],
                            taxonomy.MUNICIPALITY: ["1111"]},
                           [
                               {"term": {"workplace_address.region_code": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.municipality_code": {"value": "1111", "boost": 2.0}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "01", "boost": 1.0}}},
                               {"term": {
                                   "workplace_address.municipality_concept_id": {"value": "1111", "boost": 2.0}}},
                           ],
                           [
                               {"term": {"workplace_address.region_code": {"value": "02"}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "02"}}}
                           ]),
                          ({settings.APIKEY: "",
                            taxonomy.REGION: ["01"],
                            taxonomy.MUNICIPALITY: ["1111", "-2222"]},
                           [
                               {"term": {"workplace_address.region_code": {"value": "01", "boost": 1.0}}},
                               {"term": {"workplace_address.municipality_code": {"value": "1111", "boost": 2.0}}},
                               {"term": {"workplace_address.region_concept_id": {"value": "01", "boost": 1.0}}},
                               {"term": {
                                   "workplace_address.municipality_concept_id": {"value": "1111", "boost": 2.0}}},
                           ],
                           [
                               {"term": {"workplace_address.municipality_code": {"value": "2222"}}},
                               {"term": {"workplace_address.municipality_concept_id": {"value": "2222"}}}
                           ])])
def test_region_municipality_query(args, expected_pos, expected_neg, mock_query_builder):
    query_dsl = mock_query_builder.parse_args(args)
    if expected_pos:
        pos_query = query_dsl["query"]["bool"]["must"][0]["bool"]["should"]
        assert (len(pos_query) == len(expected_pos))
        for e in expected_pos:
            assert (e in pos_query)
    if expected_neg:
        neg_query = query_dsl["query"]["bool"]['must'][0]["bool"]["must_not"]
        assert (len(neg_query) == len(expected_neg))
        for e in expected_neg:
            assert (e in neg_query)


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
def test_rewrite_querystring(mock_query_builder):
    # concepts blob should be handled differently
    concepts = {'skill': [
        {'term': 'c++', 'uuid': '1eb1dbeb-e22a-53cb-bb28-c9fbca5ad307',
         'concept': 'C++', 'type': 'KOMPETENS',
         'term_uuid': '9734cba6-eff8-5cdc-9881-392a4345e57e',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'c#', 'uuid': 'af98ee4d-49e7-5274-bc76-a9f119c1514c',
         'concept': 'C-sharp', 'type': 'KOMPETENS',
         'term_uuid': '37da571a-a958-5b3d-a857-0a0a6bbc88cf',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'asp.net', 'uuid': '18d88a83-55d5-527b-a800-3695ed035a0c',
         'concept': 'Asp.net', 'type': 'KOMPETENS',
         'term_uuid': '280d3fa7-becd-510d-94ac-c67edb0ef4e0',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'c++', 'uuid': '1eb1dbeb-e22a-53cb-bb28-c9fbca5ad307',
         'concept': 'C++', 'type': 'KOMPETENS',
         'term_uuid': '9734cba6-eff8-5cdc-9881-392a4345e57e',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'tcp/ip', 'uuid': '09df5ef2-357f-5cfc-9333-dec2e220638a',
         'concept': 'Tcp/ip', 'type': 'KOMPETENS',
         'term_uuid': 'a18b2945-779f-5032-bbaa-c7945a63055f',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''}], 'occupation': [
        {'term': 'specialpedagog',
         'uuid': '4872acf8-ea61-50fe-8a7e-7af82b37ce9e',
         'concept': 'Specialpedagog',
         'type': 'YRKE', 'term_uuid': 'c6db8f6e-69f7-5aae-af18-2a1eae084eba',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'lärare', 'uuid': 'eadc9f5f-35c0-5324-b215-ea388ca054ff',
         'concept': 'Lärare', 'type': 'YRKE',
         'term_uuid': '300844f7-77b6-539e-a8d7-1955ce18a00c',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''},
        {'term': 'speciallärare',
         'uuid': '2708c006-d8d0-5920-b434-a5968aa088e3',
         'concept': 'Speciallärare',
         'type': 'YRKE', 'term_uuid': 'cd50806f-3c52-5e73-a06e-c7a65f7410a4',
         'term_misspelled': False,
         'version': 'NARVALONTOLOGI-2.0.0.33', 'operator': ''}], 'trait': [],
        'location': [], 'skill_must': [],
        'occupation_must': [], 'trait_must': [], 'location_must': [],
        'skill_must_not': [],
        'occupation_must_not': [], 'trait_must_not': [],
        'location_must_not': []}
    assert mock_query_builder._rewrite_querystring("specialpedagog lärare speciallärare", concepts) == ""
    assert mock_query_builder._rewrite_querystring("specialpedagog speciallärare lärare", concepts) == ""
    assert mock_query_builder._rewrite_querystring("lärare speciallärare flärgare", concepts) == "flärgare"
    assert mock_query_builder._rewrite_querystring("korvprånglare c++ asp.net [python3] flärgare",
                                                   concepts) == "korvprånglare [python3] flärgare"
    assert mock_query_builder._rewrite_querystring("tcp/ip", concepts) == ""


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("querystring, expected_phrase, expected_returned_query, test_id", [
    # With these quotes, the query will be returned with some quote modification
    # the 'matches' field will be empty
    ("'gymnasielärare'", [], 'gymnasielärare', 'a'),
    ("""gymnasielärare""", [], 'gymnasielärare', 'b'),
    ('''gymnasielärare''', [], 'gymnasielärare', 'c'),
    ("gymnasielärare\"", [], 'gymnasielärare', 'd'),  #
    ("gymnasielärare\"", [], 'gymnasielärare', 'd2'),  #
    ("gymnasielärare\'", [], 'gymnasielärare', 'e'),
    ("\'gymnasielärare", [], 'gymnasielärare', 'f'),
    (r"""gymnasielärare""", [], 'gymnasielärare', 'g'),
    (r'''gymnasielärare''', [], 'gymnasielärare', 'h'),
    ("gymnasielärare lärare", [], 'gymnasielärare lärare', 'i'),
    ("""'gymnasielärare'""", [], 'gymnasielärare', 'j'),

    ('''"gymnasielärare" "lärare"''', ['gymnasielärare', 'lärare'], '', 'aa'),
    ('''"gymnasielärare lärare"''', ['gymnasielärare lärare'], '', 'ab'),
    ('"gymnasielärare"', ['gymnasielärare'], '', 'ac'),
    ("\"gymnasielärare\"", ['gymnasielärare'], '', 'ad'),
    ("\"gymnasielärare", ['gymnasielärare'], '', 'ae'),
    ("\"gymnasielärare", ['gymnasielärare'], '', 'af'),
    ('''"gymnasielärare"''', ['gymnasielärare'], '', 'ag'),

    # "normal" quotes, 'phrases' field empty, query returned
    ("gymnasielärare", [], 'gymnasielärare', 'x'),
    ('gymnasielärare', [], 'gymnasielärare', 'y'),
    ('python', [], 'python', 'z'),
])
def test_extract_querystring_different_quotes(querystring, expected_phrase, expected_returned_query, test_id,
                                              mock_query_builder):
    """
        Test behavior of querybuilder.extract_quoted_phrases
        when sending strings with different types of quotes
    """
    actual_result = mock_query_builder.extract_quoted_phrases(querystring)

    # no plus or minus used in this test, so these fields must be empty
    assert actual_result[0]['phrases_must'] == [], f"'phrases_must' was {actual_result[0]['phrases_must']}"
    assert actual_result[0]['phrases_must_not'] == [], f"'phrases_must_not' was {actual_result[0]['phrases_must_not']}"

    actual_phrases = actual_result[0]['phrases']
    assert actual_phrases == expected_phrase, f"got {actual_phrases} but expected {expected_phrase}"

    actual_returned_query = actual_result[1]
    assert actual_returned_query == expected_returned_query, f"got {actual_returned_query} but expected {expected_returned_query}"


@pytest.mark.parametrize("mock_query_builder", all_query_builders)
@pytest.mark.parametrize("querystring, expected", [
    ("-php", {"bool": {"must_not": {"term": {"keywords.enriched.skill.raw": {"value": "php"}}}}}),
    ("+java", {"bool": {"must": {"term": {"keywords.enriched.skill.raw": {"value": "java"}}}}}),
    ("python",
     {"bool": {"must": {
         "bool": {"should": {"term": {"keywords.enriched.skill.raw": {"value": "python"}}}}}}}),
    ("systemutvecklare python +java",
     {"bool": {
         "must": {"bool": {"should": {"term": {"keywords.enriched.skill.raw": {"value": "python"}}}}}}}),
    ("systemutvecklare python +java",
     {"bool": {"must": {"term": {"keywords.enriched.skill.raw": {"value": "java"}}}}}),
    ("systemutvecklare python +java", {"bool": {
        "must": {"bool": {
            "should": {"term": {"keywords.enriched.occupation.raw": {"value": "systemutvecklare"}}}}}}}),
    ("systemutvecklare python +java", {"bool": {
        "must": {"bool": {
            "should": {"term": {"keywords.extracted.occupation.raw": {"value": "systemutvecklare"}}}}}}}),
    ("systemutvecklare python +java -php",
     {"bool": {
         "must": {"bool": {"should": {"term": {"keywords.enriched.skill.raw": {"value": "python"}}}}}}}),
    ("systemutvecklare python +java -php",
     {"bool": {"must": {"term": {"keywords.enriched.skill.raw": {"value": "java"}}}}}),
    ("systemutvecklare python +java -php", {"bool": {
        "must": {"bool": {
            "should": {"term": {"keywords.enriched.occupation.raw": {"value": "systemutvecklare"}}}}}}}),
    ("systemutvecklare python +java -php",
     {"bool": {"must_not": {"term": {"keywords.enriched.skill.raw": {"value": "php"}}}}}),
])
def test_freetext_bool_structure(querystring, expected, mock_query_builder):
    result = mock_query_builder._build_freetext_query(querystring, queryfields=None, freetext_bool_method="and",
                                                      disable_smart_freetext=False)
    assert _assert_json_structure(result, expected)
