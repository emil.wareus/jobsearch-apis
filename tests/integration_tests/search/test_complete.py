import pytest
import logging
from sokannonser.repository.querybuilder import QueryBuilder
from sokannonser.repository.typeahead import _check_search_word_type


@pytest.mark.parametrize("search_text, expected_result", [
    ('', None),
    ('stockholm', 'location'),
    ('"stockholm', None),
    ('städare &', None),  # could be improved to return 'occupation', but this is what we get now (and no 400)
])
def test_check_search_word_type_nar_862_and_1107(caplog, search_text, expected_result):
    """
    Tests for search word type
    """
    result = _check_search_word_type(search_text, QueryBuilder())
    log_levels = [tup[1] for tup in caplog.record_tuples]
    assert logging.ERROR not in log_levels, "Logging error from elastic call"
    assert logging.WARNING not in log_levels, "Logging warning from elastic call"
    assert result == expected_result
