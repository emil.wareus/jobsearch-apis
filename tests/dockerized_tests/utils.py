import datetime
import os
import subprocess
import pathlib

from tests.dockerized_tests.definitions import unit_tests, integration_tests, api_tests
from tests.dockerized_tests.docker_handler import DockerHandler
from tests.dockerized_tests.settings import report_dir_name, COLLECT_ONLY, compose_base_commands

test_env = os.environ.copy()
test_results = []


def create_report_dir():
    p = pathlib.Path(file_dir() / report_dir_name)
    p.mkdir(parents=True, exist_ok=True)


def file_dir():
    return pathlib.Path(__file__).resolve().parent


def write_results(test_name, result):
    test_name = test_name.replace(' ', '_')
    report_dir_path = file_dir() / report_dir_name
    file_name = f"{test_name}_stdout.txt"
    header = f"{datetime.datetime.now()}\n"
    with open(report_dir_path / file_name, mode='w') as f:
        f.write(header)
        f.write(result.stdout)
    if result.stderr:
        file_name_stderr = f"{test_name}_stderr.txt"
        with open(report_dir_path / file_name_stderr, mode='w') as f:
            f.write(header)
            f.write(result.stderr)


def check_result(test_results):
    print("\nTest results:")
    successes = []
    for item in test_results:
        print(f"{item['test_name']}: {result_to_text(item['test_return_code'])}")
    for code in successes:
        assert code == 0, "test suite failed"


def result_to_text(result):
    if result == 0:
        return 'SUCCESS'
    else:
        return 'FAILED'


class Timer():
    def __init__(self, msg):
        self.start = datetime.datetime.now()
        self.msg = msg

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        print(f"{self.msg}: {datetime.datetime.now() - self.start}")


def run_tests(test_object):
    with Timer(test_object['name']):
        path_to_tests = test_object['path']
        print(f"Tests in {path_to_tests} started")

        test_command = ["pytest", path_to_tests]
        if COLLECT_ONLY:
            # just collects the test but don't run them for quicker feedback on the Docker setup/teardown
            test_command = ["pytest", "--collect-only", path_to_tests]

        result = subprocess.run(test_command, env=test_env, capture_output=True, text=True)

        write_results(test_object['name'], result)
        print(f"Tests in {path_to_tests}: {result_to_text(result.returncode)}")
        test_results.append({
            'test_name': test_object['name'],
            'test_return_code': result.returncode
        })


def run_api_tests(list_of_tests):
    app = list_of_tests[0]['app']
    test_env['flask_app'] = app
    with DockerHandler(app):
        for test_object in list_of_tests:
            run_tests(test_object)


def run_all():
    with Timer("run all"):
        for test_object in [unit_tests, integration_tests]:
            run_tests(test_object)

        for app_tests in api_tests:
            run_api_tests(app_tests)


def build():
    with Timer("build"):
        commands = compose_base_commands.copy()
        commands.extend(["build"])
        print(f"Docker command: {' '.join(commands)}")
        r = subprocess.run(commands, capture_output=True, text=True, env=test_env)
        assert r.returncode == 0
        write_results("docker build", r)
