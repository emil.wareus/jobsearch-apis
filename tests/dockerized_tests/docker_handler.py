
import os
import sys

import datetime
import subprocess
import time
import requests

from tests.dockerized_tests.settings import API_KEY, compose_base_commands


class DockerHandler():
    """
    starts the Docker container (in init)
    and stops it when leaving the context (exit)
    """

    def __init__(self, app):
        self.app = app
        self.start = datetime.datetime.now()
        self.env = os.environ.copy()
        self.env['TEST_API_KEY'] = API_KEY
        self.env['FLASK_APP'] = self.app
        self.exit_if_already_running()
        print(f"Starting {self.app}")
        self.compose_up()
        self.is_app_upp(retries=20)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.compose_down()
        elapsed = datetime.datetime.now() - self.start
        print(elapsed)

    def compose_up(self):
        commands = compose_base_commands.copy()
        commands.extend(["up", "-d"])
        self._compose(commands)

    def compose_down(self):
        commands = compose_base_commands.copy()
        commands.extend(["down"])
        self._compose(commands)

    def _compose(self, commands):
        print(f"Docker command: {' '.join(commands)}")
        r = subprocess.run(commands, env=self.env)
        assert r.returncode == 0

    def exit_if_already_running(self):
        """
        if app is already running on same address and port, the tests will not work
        no attempt to stop it, just exit
        """
        if self.is_app_upp(retries=1):
            print(f"App is already running, stop it")
            sys.exit(-1)

    def is_app_upp(self, retries):
        print(f"Check if {self.app} is running")
        start = datetime.datetime.now()
        r = None

        for i in range(retries):
            try:
                r = requests.get('http://127.0.0.1:5000')
            except Exception as e:
                print(f"  RETRY after error: {e}")
                time.sleep(3)
            else:
                r.raise_for_status()
                elapsed = datetime.datetime.now() - start
                print(f"{self.app} has started after {elapsed.seconds} seconds")
                return True
        return False


