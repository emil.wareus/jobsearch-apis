from tests.dockerized_tests.settings import root_dir

test_dir = root_dir / "tests"

unit_tests = {
    "name": "unit tests",
    "test_name": "unit_tests",
    'path': test_dir / "unit_tests",
    "result": 1,

    'api': False,
    'flask_app': ''
}

integration_tests = {
    "name": 'integration tests',
    "test_name": 'integration_tests',
    'path': test_dir / "integration_tests" / "search",
    "result": 1,

    'api': False,
    'flask_app': ''
}


jobsearch = [
    {"name": "api tests jobsearch",
     'path': test_dir / "api_tests" / "searching" / "jobsearch",
     "result": 1,
     "app": "jobsearch",
     'api': True,
     'flask_app': 'jobsearch'
     },
    {
        "name": "api tests jobsearch common",
        'path': test_dir / "api_tests" / "searching" / "common",
        "result": 1,

        "app": "jobsearch",
        'api': True,
        'flask_app': 'jobsearch'},
]

historical = [{
        'path': test_dir / "api_tests" / "searching" / "common",
        "result": 1,
        "name": "api tests historical common",
        "app": "historical",
        'api': True,
        'flask_app': 'historical'},
    {
        'path': test_dir / "api_tests" / "searching" / "historical",
        "result": 1,
        "name": "api tests historical",
        "app": "historical",
        'api': True,
        'flask_app': 'historical'}]

jobstream = [{
        'path': test_dir / "api_tests" / "jobstream",
        "result": 1,
        "name": "api tests jobstream",
        "app": "jobstream",
        'api': True,
        'flask_app': 'jobstream'}]

api_tests = [jobsearch, historical, jobstream]


