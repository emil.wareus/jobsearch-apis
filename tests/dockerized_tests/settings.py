import os
from tests.dockerized_tests.path import this_files_directory

API_KEY = os.getenv("API_KEY", '')
report_dir_name = "pytest_output"
compose_file = 'docker-compose.development.yml'

root_dir = this_files_directory().parent.parent
compose_file_with_path = root_dir / compose_file

compose_base_commands = ["docker-compose", "-f", str(compose_file_with_path)]

COLLECT_ONLY = False
