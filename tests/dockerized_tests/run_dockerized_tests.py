from tests.dockerized_tests.settings import API_KEY
from tests.dockerized_tests.utils import check_result, Timer, create_report_dir, test_env, test_results, run_all, build
from tests.dockerized_tests.compose_file_handler import ComposeFileHandler

test_env['TEST_API_KEY'] = API_KEY
test_env['FLASK_APP'] = 'jobsearch'

if __name__ == '__main__':
    with ComposeFileHandler(), Timer("Whole test suite including docker build"):
        create_report_dir()
        build()
        run_all()
        check_result(test_results)
