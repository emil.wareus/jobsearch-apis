from shutil import copyfile
from tests.dockerized_tests.settings import compose_file_with_path, this_files_directory, compose_file


class ComposeFileHandler():
    """
    copy compose-file from dockerized_tests folder to root of repository
    delete it from repo root at the end of the test suite
    """

    def __init__(self):
        self.copy_compose_file_to_root()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.delete_compose_file_from_root()

    def copy_compose_file_to_root(self):
        src = this_files_directory() / compose_file
        dst = compose_file_with_path
        r = copyfile(src, dst)
        assert r == compose_file_with_path
        print(f"Copied '{compose_file}' to '{compose_file_with_path}'")

    def delete_compose_file_from_root(self):
        try:
            compose_file_with_path.unlink(missing_ok=True)
            print(f"Deleted {compose_file_with_path}")
        except Exception as e:
            print(f"ERROR when deleting: '{compose_file_with_path}' {e}")
        finally:
            pass
