from tests.test_resources.helper import compare_two_lists

all_keys = ['occupation-name', 'occupation-group', 'occupation-field', 'employment-type', 'country', 'region',
            'municipality', 'language', 'skill']

all_stats = ['occupation-name', 'occupation-group', 'municipality', 'region', 'country']

default_values = 10


def compare_keys(stats):
    keys = []
    for item in stats:
        keys.append(item)
    compare_two_lists(keys, all_keys)


def check_default_values(stats):
    for key, value in stats.items():
        expected = 10  # default
        if key == 'employment-type':
            expected = 6
        assert len(value) == expected


expected_top_values = {
    'occupation': 392,
    'occupation_group': 396,
    'occupation_field': 971,
    'employment_type': 4389,
    'country': 4999,
    'region': 1227,
    'municipality': 775,
    'language': 490,
    'skill': 23}
