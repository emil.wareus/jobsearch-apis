def ad_ids_from_response(response):
    all_ids = []
    for hit in response['hits']:
        all_ids.append(hit['id'])
    return all_ids
