# number is expected value on static test data
from common.settings import REMOTE, ABROAD, EXPERIENCE_REQUIRED, UNSPECIFIED_SWEDEN_WORKPLACE, MUNICIPALITY, \
    OCCUPATION, OCCUPATION_GROUP, OCCUPATION_FIELD
from tests.test_resources.concept_ids import concept_ids_geo as geo
from tests.test_resources.concept_ids import occupation, occupation_group, occupation_field
from tests.test_resources.test_settings import NUMBER_OF_ADS, EXPECTED_GYMNASIE_LARARE

Q = 'q'
REGION = 'region'
COUNTRY = 'country'

test_cases_param = [
    ({}, NUMBER_OF_ADS),
    ({Q: 'gymnasielärare'}, EXPECTED_GYMNASIE_LARARE),
    ({Q: 'lärare'}, 254),
    ({Q: 'C#'}, 47),
    ({Q: 'c-körkort'}, 30),
    ({Q: '.net'}, 38),
    ({Q: 'ci/cd'}, 22),
    ({Q: 'erp-system'}, 7),
    ({Q: 'tcp/ip'}, 8),
    ({Q: 'cad-verktyg'}, 1),
    ({Q: 'backend-utvecklare'}, 19),
    ({Q: 'it-tekniker'}, 24),
    ({Q: 'sjuksköterska'}, 494),
    ({Q: 'sjuksköterskor'}, 494),
    ({Q: 'körkort'}, 1070),
    ({Q: 'distributör'}, 3),
    ({Q: 'livsmedel'}, 22),
    ({Q: 'sköterska'}, 1),
    ({Q: 'skötare'}, 5),
    ({Q: 'undersköterska'}, 126),
    ({Q: 'lärarutbildning'}, 16),
    ({Q: 'datasystem'}, 5),
    ({Q: 'undervisning'}, 206),
    ({Q: 'försäkring'}, 65),
    ({Q: 'barnmorska'}, 17),
    ({Q: 'tandsköterska'}, 20),
    ({Q: 'kock'}, 79),
    ({Q: 'stockholm'}, 844),
    ({Q: 'göteborg'}, 387),
    ({Q: 'malmö'}, 231),
    ({Q: 'uppsala'}, 123),
    ({Q: 'utvecklare'}, 166),
    ({Q: 'pizzabagare'}, 19),
    ({Q: 'personlig assistent'}, 195),
    ({Q: 'förskollärare'}, 56),
    ({Q: 'python'}, 76),
    ({Q: 'java'}, 70),
    ({Q: 'helsingborg'}, 88),
    ({Q: 'barnmorska tandsköterska'}, 37),
    ({Q: 'tandsköterska barnmorska'}, 37),
    ({Q: 'barnmorska fysioterapeut'}, 49),
    ({Q: 'chefsbarnmorska fysioterapeut'}, 32),
    ({Q: 'chefsbarnmorska barnmorska'}, 17),
    ({Q: 'developer engineer'}, 23),
    ({Q: 'python'}, 76),
    ({Q: 'java'}, 70),
    ({Q: 'java -javautvecklare'}, 62),
    ({Q: 'java python'}, 129),
    ({Q: 'java +python'}, 76),
    ({Q: 'java -python'}, 53),
    ({Q: 'kock -bagare'}, 79),
    ({Q: 'bartender'}, 4),
    ({Q: 'personlig assistent'}, 195),
    ({Q: 'personlig assistent +göteborg'}, 11),
    ({Q: 'personlig assistent -göteborg'}, 184),
    ({Q: 'förskollärare'}, 56),
    ({Q: 'sjuksköterska -stockholm'}, 431),
    ({MUNICIPALITY: ['AvNB_uwa_6n6', 'oYPt_yRA_Smm']}, 983),
    ({MUNICIPALITY: 'AvNB_uwa_6n6'}, 775),
    ({MUNICIPALITY: 'oYPt_yRA_Smm'}, 208),
    ({MUNICIPALITY: 'PVZL_BQT_XtL'}, 366),
    ({MUNICIPALITY: 'QiGt_BLu_amP'}, 61),
    ({MUNICIPALITY: 'PVZL_BQT_XtL', REGION: 'CifL_Rzy_Mku'}, 1593),
    ({MUNICIPALITY: 'PVZL_BQT_XtL', REGION: 'CifL_Rzy_Mku', 'country': 'QJgN_Zge_BzJ'}, 1603),
    ({MUNICIPALITY: 'PVZL_BQT_XtL', REGION: 'CifL_Rzy_Mku', 'country': 'i46j_HmG_v64'}, 4999),
    ({REGION: 'g5Tt_CAV_zBd'}, 113),
    ({REGION: 'CaRE_1nn_cSU'}, 643),
    ({REGION: 'CifL_Rzy_Mku'}, 1227),
    ({COUNTRY: 'i46j_HmG_v64'}, 4999),
    ({COUNTRY: 'QJgN_Zge_BzJ'}, 10),
    ({OCCUPATION: occupation.sjukskoterska__grundutbildad}, 392),
    ({OCCUPATION_GROUP: occupation_group.barnmorskor}, 16),
    ({OCCUPATION_GROUP: occupation_group.tandskoterskor}, 18),
    ({OCCUPATION_GROUP: occupation_group.kockar_och_kallskankor}, 73),
    ({OCCUPATION_GROUP: occupation_group.grundutbildade_sjukskoterskor}, 396),
    ({OCCUPATION_FIELD: occupation_field.naturbruk}, 58),
    ({OCCUPATION_FIELD: occupation_field.hotell__restaurang__storhushall}, 213),
    ({OCCUPATION_FIELD: occupation_field.transport}, 175),
    ({OCCUPATION_FIELD: occupation_field.kultur__media__design}, 38),
    ({Q: 'lärare', 'published-after': '2019-01-16T07:29:52'}, 254),
    ({Q: 'lärare', 'published-after': '2020-12-06T07:29:52'}, 252),
    ({Q: 'Kundtjänstmedarbetare', 'sort': 'pubdate-asc'}, 66),
    ({Q: 'Kundtjänstmedarbetare', 'sort': 'pubdate-desc'}, 66),
    ({Q: 'lärare', 'published-after': '2019-01-16T07:29:52', 'country': geo.sverige, 'sort': 'pubdate-asc'}, 254),
    ({Q: 'lärare', REGION: [geo.stockholms_lan, geo.skane_lan]}, 66),
    ({Q: 'lärare', REGION: geo.stockholms_lan}, 37),
    ({Q: 'lärare', REGION: geo.skane_lan}, 29),
    ({Q: 'lärare', MUNICIPALITY: geo.vasteras}, 1),
    ({Q: 'lärare', MUNICIPALITY: geo.vaggeryd}, 0),

    # vanlig anställning, old and replaced_by
    ({'employment-type': 'PFZr_Syz_cUq'}, 4389),
    ({'employment-type': 'kpPX_CNN_gDU'}, 4389),
    #   sommarjobb, old and replaced_by
    ({'employment-type': 'Jh8f_q9J_pbJ'}, 360),
    ({'employment-type': 'EBhX_Qm2_8eX'}, 360),

    ({EXPERIENCE_REQUIRED: True}, 3938),
    ({EXPERIENCE_REQUIRED: False}, 1091),

    ({REMOTE: True}, 36),
    ({REMOTE: True, MUNICIPALITY: geo.stockholm}, 13),
    ({REMOTE: True, REGION: geo.stockholms_lan}, 14),
    ({REMOTE: True, ABROAD: True}, 0),
    ({REMOTE: True, EXPERIENCE_REQUIRED: True}, 31),
    ({REMOTE: True, OCCUPATION_GROUP: occupation_group.mjukvaru__och_systemutvecklare_m_fl_}, 3),
    ({REMOTE: True, OCCUPATION_FIELD: occupation_field.forsaljning__inkop__marknadsforing}, 6),
    ({REMOTE: True, Q: 'Stockholms Län'}, 14),
    ({REMOTE: True, Q: 'Stockholm'}, 13),
    ({REMOTE: True, Q: 'utvecklare'}, 2),

    # from "manual test cases"
    ({REGION: '90'}, 0),
    ({REGION: '09'}, 32),
    ({UNSPECIFIED_SWEDEN_WORKPLACE: True}, 135),
    ({REGION: '90', UNSPECIFIED_SWEDEN_WORKPLACE: True}, 135),
    ({COUNTRY: '219'}, 0),  # Germany
    ({REGION: '90', COUNTRY: '219'}, 0),  # Germany
    ({UNSPECIFIED_SWEDEN_WORKPLACE: True, COUNTRY: '-199'}, 0),
]
