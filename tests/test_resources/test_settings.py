import datetime
import os
from common import settings

# environment variables must be set
TEST_USE_STATIC_DATA = os.getenv('TEST_USE_STATIC_DATA', True)
test_api_key = os.getenv('TEST_API_KEY')

NUMBER_OF_ADS = 5029
DAWN_OF_TIME = '1971-01-01T00:00:01'
current_time_stamp = datetime.datetime.now().strftime(settings.DATE_FORMAT)

test_headers = {'api-key': test_api_key, 'accept': 'application/json'}

test_host = os.getenv('TEST_HOST', 'http://127.0.0.1')
test_port = os.getenv('TEST_PORT', 5000)
TEST_URL = f"{test_host}:{test_port}"

EXPECTED_GYMNASIE_LARARE = 60

REMOTE_MATCH_PHRASES = [y.lower() for y in
                        ["Arbeta på distans", "Arbete på distans", "Jobba på distans", "Arbeta hemifrån",
                         "Arbetar hemifrån", "Jobba hemifrån", "Jobb hemifrån", "remote work", "jobba tryggt hemifrån",
                         "work remote", "jobba remote", "arbeta remote"]]

OPEN_FOR_ALL_PHRASE = ["öppen för alla"]

TRAINEE_PHRASES = ["rollen som trainee",
                   "söker trainee",
                   "som trainee",
                   "anställningsvillkor Trainee",
                   "as a trainee",
                   "anställer trainee"]

LARLING_PHRASES = ["som lärling",
                   "lärling sökes",
                   "lärlingsplats",
                   "lärlingstjänst",
                   "och lärling"]

FRANCHISE_PHRASES = ["franchisetagare sökes",
             "rollen som franchisetagare",
             "som franchisetagare",
             "franchisetagare/egen företagare"]

HIRE_WORKPLACE_PHRASES = ["anställa/hyra stol",
                          "hyra stol/plats",
                          "hyr du stol",
                          "uthyrning av stol",
                          "uthyrning stol",
                          "hyra stol",
                          "hyrstol",
                          "frisörstol"]
