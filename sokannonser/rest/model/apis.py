from flask_restx import Api, Namespace, fields
from common import settings
from common.result_model import root_api, job_ad

# Common result models for both Jobsearch and Historical ads

job_ad_searchresult = root_api.inherit('JobAdSearchResult', job_ad, {
    'relevance': fields.Float(),
})

stat_item = root_api.model('StatDetail', {
    'term': fields.String(),
    'concept_id': fields.String(),
    'code': fields.String(),
    'count': fields.Integer()
})

search_stats = root_api.model('Stats', {
    'type': fields.String(),
    'values': fields.List(fields.Nested(stat_item, skip_none=True))
})

freetext_concepts = root_api.model('FreetextConcepts', {
    'skill': fields.List(fields.String()),
    'occupation': fields.List(fields.String()),
    'location': fields.List(fields.String()),
    'skill_must': fields.List(fields.String()),
    'occupation_must': fields.List(fields.String()),
    'location_must': fields.List(fields.String()),
    'skill_must_not': fields.List(fields.String()),
    'occupation_must_not': fields.List(fields.String()),
    'location_must_not': fields.List(fields.String()),
})

number_of_hits = root_api.model('NumberOfHits', {
    'value': fields.Integer()
})

open_results = root_api.model('SearchResults', {
    'total': fields.Nested(number_of_hits),
    'positions': fields.Integer(),
    'query_time_in_millis': fields.Integer(),
    'result_time_in_millis': fields.Integer(),
    'stats': fields.List(fields.Nested(search_stats, skip_none=True)),
    'freetext_concepts': fields.Nested(freetext_concepts, skip_none=True),
    'hits': fields.List(fields.Nested(job_ad_searchresult), attribute='hits', skip_none=True)
})

# Jobsearch

search_api = Api(version=settings.API_VERSION, title='Search job ads',
                 description='An API for searching and retrieving job ads and for finding '
                             'concepts in the Jobtech Taxonomy.',
                 default_label="An API for searching and retrieving job ads.")

ns_platsannons = Namespace('Open AF-job ads',
                           description='Search and retrieve Arbetsförmedlingens (AF) '
                                       'job ads. Used for online operations.')

ns_valuestore = Namespace('Jobtech Taxonomy',
                          description=settings.TAX_DESCRIPTION)

search_api.add_namespace(ns_platsannons, '/')
search_api.add_namespace(ns_valuestore, '/taxonomy')

for name, definition in root_api.models.items():
    ns_platsannons.add_model(name, definition)

typeahead_item = ns_platsannons.model('TypeaheadItem', {
    'value': fields.String(),
    'found_phrase': fields.String(),
    'type': fields.String(),
    'occurrences': fields.Integer()
})

typeahead_results = ns_platsannons.model('TypeaheadResults', {
    'result_time_in_millis': fields.Integer(),
    'time_in_millis': fields.Integer(),
    'typeahead': fields.List(fields.Nested(typeahead_item))
})

# Historical ads

historical_api = Api(version=settings.API_VERSION, title='Historical job ads',
                     description='An API for searching and retrieving historical job ads.',
                     default_label="An API for searching and retrieving historical job ads.")

ns_historical = Namespace('Historical ads',
                          description='Search and retrieve historical job ads from '
                                      'Arbetsförmedlingen since 2006. Used for online operations.')

historical_api.add_namespace(ns_historical, '/')

for name, definition in root_api.models.items():
    ns_historical.add_model(name, definition)
