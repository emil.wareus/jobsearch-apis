import logging
import time
from flask_restx import abort
from flask_restx import Resource
from jobtech.common.rest.decorators import check_api_key_and_return_metadata
from common import settings
from sokannonser.repository.querybuilder import QueryBuilderType
from sokannonser.rest.endpoint.common import search_endpoint, QueryBuilderContainer, \
    format_hits_with_only_original_values
from sokannonser.rest.model.apis import ns_platsannons, open_results, job_ad, typeahead_results
from sokannonser.rest.model.queries import annons_complete_query, annons_search_query, load_ad_query
from sokannonser.rest.model.swagger import swagger_doc_params, swagger_filter_doc_params
from sokannonser.repository.platsannonser import fetch_platsannons
from sokannonser.repository.logo import fetch_ad_logo
from sokannonser.repository.typeahead import suggest, suggest_extra_word
import elasticapm

log = logging.getLogger(__name__)

querybuilder_container = QueryBuilderContainer()


@ns_platsannons.route('ad/<id>', endpoint='ad')
class AdById(Resource):
    method_decorators = [check_api_key_and_return_metadata('pb')]

    @ns_platsannons.doc(
        description='Load a job ad by ID',
    )
    @ns_platsannons.response(401, 'Invalid API-key')
    @ns_platsannons.response(404, 'Job ad not found')
    @ns_platsannons.expect(load_ad_query)
    @ns_platsannons.marshal_with(job_ad)
    def get(self, id, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        result = fetch_platsannons(str(id))
        if result.get('removed'):
            abort(404, 'Ad not found')
        else:
            return format_hits_with_only_original_values([result])[0]


@ns_platsannons.route('ad/<id>/logo', endpoint='ad_logo')
class AdLogo(Resource):
    @ns_platsannons.doc(
        description='Load a logo binary file by ID',
    )
    @ns_platsannons.response(404, 'Job ad not found')
    def get(self, id):
        return fetch_ad_logo(str(id))


@ns_platsannons.route('search')
class Search(Resource):
    method_decorators = [check_api_key_and_return_metadata('pb')]

    @ns_platsannons.doc(
        description='Search using parameters and/or freetext',
        params={**swagger_doc_params, **swagger_filter_doc_params},
    )
    @ns_platsannons.response(401, 'Invalid API key')
    @ns_platsannons.expect(annons_search_query)
    @ns_platsannons.marshal_with(open_results)
    def get(self, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        start_time = int(time.time() * 1000)
        args = annons_search_query.parse_args()
        return search_endpoint(querybuilder_container.get(QueryBuilderType.JOBSEARCH_SEARCH), args, start_time)


@ns_platsannons.route('complete')
class Complete(Resource):
    method_decorators = [check_api_key_and_return_metadata('pb')]

    @ns_platsannons.doc(
        description='Typeahead / Suggest next searchword',
        params={
            settings.CONTEXTUAL_TYPEAHEAD: "Set to False to disable contextual typeahead (default: True)",
            **swagger_doc_params
        }
    )
    @ns_platsannons.response(401, 'Invalid API-key')
    @ns_platsannons.expect(annons_complete_query)
    @ns_platsannons.marshal_with(typeahead_results)
    def get(self, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        start_time = int(time.time() * 1000)
        args = annons_complete_query.parse_args()
        limit = args[settings.LIMIT] \
            if args[settings.LIMIT] <= settings.MAX_COMPLETE_LIMIT else settings.MAX_COMPLETE_LIMIT

        word_list, space_last = self.typeahead_query_analyzer(args.get(settings.FREETEXT_QUERY))
        prefix_words = word_list if space_last else word_list[:-1]
        args[settings.TYPEAHEAD_QUERY] = ' '.join(word_list) + (' ' if space_last else '')  # used for aggs queries
        args[settings.FREETEXT_QUERY] = ' '.join(prefix_words)  # used for subset selections (if contextual==True)

        querybuilder_complete = querybuilder_container.get(QueryBuilderType.JOBSEARCH_COMPLETE)
        result = suggest(args, querybuilder_complete)

        # if only one suggestion was given: find extra words
        if len(result.get('aggs')) == 1:
            log.debug(f"Typeahead result with only one suggestion, so adding extra words.")
            extra_words = suggest_extra_word(result.get('aggs')[0]['value'].strip(),
                                             querybuilder_complete)
            result['aggs'] += extra_words

        # TODO: This one can only happen inside above if-clause?
        # if <word> + <space>: make sure <word> is deleted from suggestions
        if len(word_list) == 1 and space_last:
            log.debug("<word><space> query pattern.")
            result['aggs'] = self.find_agg_and_delete(word_list[0], result['aggs'])

        log.debug(f"Typeahead final result: {result['aggs']}")
        log.debug(f"Typeahead query results after: {int(time.time() * 1000 - start_time)} milliseconds.")

        return self.marshal_results(result, limit, start_time)

    @staticmethod
    def typeahead_query_analyzer(original_query):
        # all unnecessary whitespaces are removed
        word_list = original_query.split() if original_query else []
        space_last = (original_query[-1] in ' ') if original_query else False  # TODO: accept also tab last? -> ' \t'
        return word_list, space_last

    @staticmethod
    def marshal_results(esresult, limit, start_time):
        typeahead_result = esresult.get('aggs', [])
        if len(typeahead_result) > limit:
            typeahead_result = typeahead_result[:limit]
        result_time = int(time.time() * 1000 - start_time)
        result = {
            "result_time_in_millis": result_time,
            "time_in_millis": esresult.get('took', 0),
            "typeahead": typeahead_result,
        }
        log.debug(f"Typeahead sending results after: {result_time} milliseconds.")
        return result

    @staticmethod
    def find_agg_and_delete(value, aggs):
        # use to find item from aggs result
        remove_agg = ''
        for agg in aggs:
            if agg['value'] == value:
                remove_agg = agg  # TODO: not executed by tests
                break
        if remove_agg:
            aggs.remove(remove_agg)
            log.debug(f'(find_agg_and_delete) Removed word from aggs: {value}')
        else:
            log.debug(f'(find_agg_and_delete) No removal needed for word: {value}. Aggs untouched.')
        return aggs
