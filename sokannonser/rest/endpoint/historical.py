import logging
import time
from flask_restx import abort
from flask_restx import Resource
from jobtech.common.rest.decorators import check_api_key_and_return_metadata

from sokannonser.repository.platsannonser import fetch_platsannons
from sokannonser.repository.querybuilder import QueryBuilderType
from sokannonser.repository.stats.stats import get_stats
from sokannonser.rest.endpoint.common import QueryBuilderContainer, \
    search_endpoint, format_hits_with_only_original_values
from sokannonser.rest.model.apis import ns_historical, open_results, job_ad
from sokannonser.rest.model.queries import load_ad_query, historical_query, stats_query
from sokannonser.rest.model.swagger import swagger_doc_params, swagger_filter_doc_params
from common import settings
import elasticapm

log = logging.getLogger(__name__)

querybuilder_container = QueryBuilderContainer()


@ns_historical.route('ad/<id>', endpoint='ad')
class AdById(Resource):
    method_decorators = [check_api_key_and_return_metadata('pb')]  # TODO: Change api_identifier?

    @ns_historical.doc(
        description='Load a job ad by ID',
    )
    @ns_historical.response(401, 'Invalid API-key')
    @ns_historical.response(404, 'Job ad not found')
    @ns_historical.expect(load_ad_query)
    @ns_historical.marshal_with(job_ad)
    def get(self, id, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        result = fetch_platsannons(str(id))
        if result.get('removed'):
            abort(404, 'Ad not found')
        else:
            return format_hits_with_only_original_values([result])[0]


@ns_historical.route('search')
class Search(Resource):
    method_decorators = [check_api_key_and_return_metadata('pb')]  # TODO: Change api_identifier?

    @ns_historical.doc(
        description='Search using parameters and/or freetext',
        params={
            settings.HISTORICAL_FROM: 'Search ads from this date',
            settings.HISTORICAL_TO: 'Search ad until this date',
            settings.START_SEASONAL_TIME: 'Search seasonal ads from this date, date type MM-DD',
            settings.END_SEASONAL_TIME: 'Search seasonal ads until this date, date type MM-DD',
            settings.HISTORICAL_REQUEST_TIMEOUT: 'Set the query timeout in seconds (some historical queries may take several minutes)',
            **swagger_doc_params, **swagger_filter_doc_params
        },
    )
    @ns_historical.response(401, 'Invalid API key')
    @ns_historical.expect(historical_query)
    @ns_historical.marshal_with(open_results)
    def get(self, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        start_time = int(time.time() * 1000)
        args = historical_query.parse_args()
        return search_endpoint(querybuilder_container.get(QueryBuilderType.HISTORICAL_SEARCH), args, start_time)


@ns_historical.route('stats')
class Stats(Resource):
    method_decorators = [check_api_key_and_return_metadata('pb')]

    @ns_historical.doc(
        description='Load stats by taxonomy type',
        params={
            settings.APIKEY: "Required API key",
            settings.TAXONOMY_TYPE: "One or more taxonomy type, default will return all (available fields: "
                                    "occupation-name, occupation-group, occupation-field, employment-type,"
                                    " country, region, municipality, skill, language). ",
            settings.STATS_BY: "Search taxonomy type with different fields",
            settings.LIMIT: "Maximum number of statistical rows per taxonomy field"
        },
    )
    @ns_historical.response(401, 'Invalid API key')
    @ns_historical.expect(stats_query)
    def get(self, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        args = stats_query.parse_args()
        return get_stats(args)
