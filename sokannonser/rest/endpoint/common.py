import logging
import time
from flask import request
from common import settings
from sokannonser.repository.text_to_concept import TextToConcept
from sokannonser.repository.querybuilder import QueryBuilder, QueryBuilderType
from sokannonser.repository.platsannonser import find_platsannonser

log = logging.getLogger(__name__)


# Endpoint functions common between Jobsearch and Historical ads

class QueryBuilderContainer(object):
    """
    Holds full blown QueryBuilder object with ontology
    Use launch() to create, and get() to get the object
    """

    def __init__(self):
        self.querybuilder = None

    def get(self, qb_type):
        if not isinstance(qb_type, QueryBuilderType):
            return None
        self.querybuilder.set_qb_type(qb_type)
        return self.querybuilder

    def launch(self):
        ttc = TextToConcept(ontologyhost=settings.ES_HOST,
                            ontologyport=settings.ES_PORT,
                            ontologyuser=settings.ES_USER,
                            ontologypwd=settings.ES_PWD)
        self.querybuilder = QueryBuilder(ttc)


def search_endpoint(querybuilder, args, start_time):
    log.debug(f"Query parsed after: {int(time.time() * 1000 - start_time)} milliseconds.")
    result = find_platsannonser(args,
                                querybuilder,
                                start_time,
                                request.headers.get('X-Fields'))

    log.debug(f"Query results after: {int(time.time() * 1000 - start_time)} milliseconds.")
    max_score = result.get('max_score', 1.0)
    hits = [dict(hit['_source'],
                 **{'relevance': (hit['_score'] / max_score)
                 if max_score > 0 else 0.0})
            for hit in result.get('hits', [])]
    hits = format_hits_with_only_original_values(hits)
    result['hits'] = hits
    return _format_api_response(result, start_time)


def _format_api_response(esresult, start_time):
    total_results = {'value': esresult.get('total', {}).get('value')}
    result_time = int(time.time() * 1000 - start_time)
    result = {
        "total": total_results,
        "positions": esresult.get('positions', 0),
        "query_time_in_millis": esresult.get('took', 0),
        "result_time_in_millis": result_time,
        "stats": esresult.get('stats', []),
        "freetext_concepts": esresult.get('concepts', {}),
        "hits": esresult.get('hits', {})
    }
    log.debug(f"Sending results after: {result_time} milliseconds.")
    return result


def format_hits_with_only_original_values(hits):
    for hit in hits:  # TODO write unit tests for None and not-None values
        if occupations := hit.get('occupation', []):
            hit['occupation'] = _keep_original_value(occupations)
        occ_group = hit.get('occupation_group', None)
        if occ_group and isinstance(occ_group, list):
            hit['occupation_group'] = occ_group[0]
        occ_field = hit.get('occupation_field', None)
        if occ_field and isinstance(occ_field, list):
            hit['occupation_field'] = occ_field[0]
        if skills_must := hit.get('must_have', {}).get('skills', None):
            hit['must_have']['skill'] = _keep_multiple_original_values_in_list(skills_must)
        if skills_nice := hit.get('nice_to_have', {}).get('skills', None):
            hit['nice_to_have']['skill'] = _keep_multiple_original_values_in_list(skills_nice)
        if employment_type := hit.get('employment_type', []):
            hit['employment_type'] = _keep_original_value(employment_type)
    return hits


def _keep_original_value(values):
    for value in values:
        if value.get('original_value', False):
            return {
                "concept_id": value.get('concept_id'),
                "label": value.get('label'),
                "legacy_ams_taxonomy_id": value.get('legacy_ams_taxonomy_id')
            }


def _keep_multiple_original_values_in_list(values):
    """
    Used where there can be one or more 'original values', e.g. skills
    """
    result = []
    for value in values:
        if value.get('original_value', False):
            result.append({
                "concept_id": value.get('concept_id'),
                "label": value.get('label'),
                "legacy_ams_taxonomy_id": value.get('legacy_ams_taxonomy_id')
            })
    return result
