from common import fields as f, settings


def create_historical_stats_aggs_query(stat, stats_by, limit):
    if stats_by == settings.LEGANCY_ID:
        return {
            "terms": {
                "field": f.stats_v2_options[stat],
                "size": limit
            }
        }
    elif stats_by == settings.CONCEPT_ID:
        return {
            "terms": {
                "field": f.stats_v2_concept_id_options[stat],
                "size": limit
            }
        }
    elif stats_by == settings.LABEL:
        return {
            "terms": {
                "field": f.stats_v2_concept_name_options[stat],
                "size": limit
            }
        }
