import collections
import logging
import concurrent.futures

from common import settings
from sokannonser.repository.stats.utils_taxonomy import get_taxonomy_info

log = logging.getLogger(__name__)


def _marshal_stats_results(stats_by, typelist, aggregations, limit):
    output = collections.defaultdict(list)
    with concurrent.futures.ThreadPoolExecutor(max_workers=settings.TAXONOMY_PROCESSES) as executor:
        values = {executor.submit(get_taxonomy_info, stats_by, type, item.get('key', None), item.get('doc_count', None))
                  for type in typelist
                  for item in aggregations.get(type, {}).get('buckets', [])[:limit]}
        for future in concurrent.futures.as_completed(values):
            try:
                data = future.result()
                output[data.get('type')].append({
                    'concept_id': data.get('concept_id', None),
                    'legacy_ams_taxonomy_id': data.get('legacy_ams_taxonomy_id', None),
                    'label': data.get('label', None),
                    'occurrences': int(data.get('occurrences', 0))
                })
            except Exception as exc:
                log.error(f'Taxonomy requests made an exception: {exc}')

    for type in typelist:
        item = output[type]
        item.sort(key=lambda x: x['occurrences'], reverse=True)

    return output
