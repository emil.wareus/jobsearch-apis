import logging
import json
import time
from flask_restx import abort
from elasticsearch import exceptions
from common import settings, fields, fields as f, taxonomy
from common.main_elastic_client import elastic_client
from common.elastic_connection_with_retries import elastic_search_with_retry

log = logging.getLogger(__name__)


def find_platsannonser(args, querybuilder, start_time=0, x_fields=None):
    if start_time == 0:
        start_time = int(time.time() * 1000)
    query_dsl = querybuilder.parse_args(args, x_fields)
    log.debug(f"(find_platsannonser) Query constructed after: {int(time.time() * 1000 - start_time)} milliseconds")

    # First pass, find highest score:
    if args.get(settings.MIN_RELEVANCE):  # TODO: not executed by tests
        max_score_query = query_dsl.copy()
        max_score_query['from'] = 0
        max_score_query['size'] = 1
        max_score_query['track_total_hits'] = False
        del max_score_query['aggs']
        del max_score_query['sort']
        max_score_result = elastic_search_with_retry(client=elastic_client(), query=max_score_query,
                                                     index=settings.ES_INDEX)
        if max_score_result:
            max_score = max_score_result.get('hits', {}).get('max_score')
            if max_score:
                query_dsl['min_score'] = (max_score - 1) * args.get(settings.MIN_RELEVANCE)
    log.info(f"ARGS: {args}")
    log.info(f"QUERY: {json.dumps(query_dsl)}")

    if args.get(settings.HISTORICAL_REQUEST_TIMEOUT):
        request_timeout = args.get(settings.HISTORICAL_REQUEST_TIMEOUT)
    else:
        request_timeout = settings.ES_DEFAULT_TIMEOUT

    query_result = elastic_search_with_retry(client=elastic_client(), query=query_dsl, index=settings.ES_INDEX, es_request_timeout=request_timeout)
    log.debug(f"(find_platsannonser) Elastic results after: {int(time.time() * 1000 - start_time)} ms, timeout: {request_timeout}")
    if not query_result:
        abort(500, 'Failed to establish connection to database')
        return

    if args.get(settings.FREETEXT_QUERY) and not args.get(settings.X_FEATURE_DISABLE_SMART_FREETEXT):
        # First remove any phrases
        (phrases, qs) = querybuilder.extract_quoted_phrases(args.get(settings.FREETEXT_QUERY))
        query_result['concepts'] = _extract_concept_from_concepts(querybuilder.text_to_concepts(qs))

    log.debug(
        f"(find_platsannonser) Elasticsearch reports took: {query_result.get('took', 0)}, timed_out: {query_result.get('timed_out', '')}")
    return transform_platsannons_query_result(args, query_result, querybuilder)


def _extract_concept_from_concepts(concepts):
    main_concepts = dict()
    for key, value in concepts.items():
        main_concepts[key] = [v['concept'].lower() for v in value]
    return main_concepts


def _format_ad(result):
    return result.get('_source')


def fetch_platsannons(ad_id):
    try:
        query_result = elastic_client().get(index=settings.ES_INDEX, id=ad_id, ignore=404)
        if query_result and '_source' in query_result:
            log.debug(f'Ad found by la id: {ad_id}')
            return _format_ad(query_result)
        else:
            ext_id_query = {
                'query': {
                    'term': {
                        fields.EXTERNAL_ID: ad_id
                    }
                }
            }
            query_result = elastic_search_with_retry(elastic_client(), ext_id_query, settings.ES_INDEX)
            hits = query_result.get('hits', {}).get('hits', [])
            if hits:  # TODO: never executed by tests
                log.debug(f'Ad found by external id: {ad_id}')
                return _format_ad(hits[0])

            log.info(f"Ad: {ad_id} not found, returning 404 message")
            abort(404, 'Ad not found')
    except exceptions.NotFoundError:
        log.exception(f'Failed to find id: {ad_id}')
        abort(404, 'Ad not found')
        return
    except exceptions.ConnectionError as e:
        log.exception(f'Failed to connect to elasticsearch: {e}')
        abort(500, 'Failed to establish connection to database')
        return


def transform_platsannons_query_stats_result(args, query_result, querybuilder, results: {}):
    if 'aggregations' in query_result:
        results['positions'] = int(query_result.get('aggregations', {})
                                   .get('positions', {}).get('value', 0))
        results['aggs'] = querybuilder.filter_aggs(query_result.get('aggregations', {}),
                                                   args.get(settings.FREETEXT_QUERY))

        for stat in args.get(settings.STATISTICS) or []:  # TODO: never executed by tests
            log.debug(f"Statistic for field: {stat}")
            if 'stats' not in results:
                results['stats'] = []
            values = []
            for b in query_result.get('aggregations', {}).get(stat, {}).get('buckets', []):
                source = b.get('id_and_name', {}).get('hits', {}).get('hits', {})[0].get('_source')
                if isinstance(source.get(f.stats_field[stat], {}), list):
                    field = source.get(f.stats_field[stat], {})[0]
                else:
                    field = source.get(f.stats_field[stat], {})
                value = {
                    "term": field.get(f.stats_label[stat], ''),
                    "concept_id": field.get(f.stats_concept_id[stat], ''),
                    "code": b['key'],
                    "count": b['doc_count']}
                values.append(value)
            results['stats'].append({"type": stat, "values": values})
    return results


def transform_platsannons_query_result(args, query_result, querybuilder):
    results = query_result.get('hits', {})
    results['took'] = query_result.get('took', 0)
    results['concepts'] = query_result.get('concepts', {})
    results = transform_platsannons_query_stats_result(args, query_result, querybuilder, results)
    _modify_results(results)
    return results


def _modify_results(results):
    for hit in results['hits']:
        try:
            hit['_source'] = _format_ad(hit)
        except KeyError:
            pass
        except ValueError:
            pass
