import logging
import io
import os
import requests
from flask import send_file
from werkzeug.exceptions import ServiceUnavailable
from common import settings
from sokannonser.repository.platsannonser import fetch_platsannons

log = logging.getLogger(__name__)

current_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

not_found_file = None


def get_correct_logo_url(ad_id):
    ad = fetch_platsannons(ad_id)

    logo_url = None

    if ad and 'employer' in ad:
        if 'workplace_id' in ad['employer'] and ad['employer']['workplace_id'] and int(
                ad['employer']['workplace_id']) > 0:  # TODO: never executed by tests
            '''
            Special logo for workplace_id for ads with source_type VIA_AF_FORMULAR or 
            VIA_PLATSBANKEN_AD or VIA_ANNONSERA (workplace_id > 0)
            '''
            workplace_id = ad['employer']['workplace_id']
            eventual_logo_url = f'{settings.COMPANY_LOGO_BASE_URL}arbetsplatser/{workplace_id}/logotyper/logo.png'
            logo_url = get_eventual_logo_url(eventual_logo_url)
        elif 'organization_number' in ad['employer'] and ad['employer']['organization_number']:
            org_number = ad['employer']['organization_number']
            eventual_logo_url = f'{settings.COMPANY_LOGO_BASE_URL}organisation/{org_number}/logotyper/logo.png'
            logo_url = get_eventual_logo_url(eventual_logo_url)
    return logo_url


def get_eventual_logo_url(eventual_logo_url):
    """
    This function makes a call to the logo endpoint to check response
    Download (if response OK to request) is made later
    """
    try:
        r = requests.head(eventual_logo_url, timeout=10)
    except requests.RequestException as e:
        """
        If there is an exception (requests.RequestException is base for several different exceptions),
        log it and raise http 503 "Service Unavailable"
        """
        log.error(f"Error for eventual logo url {eventual_logo_url}: {e}")
        raise ServiceUnavailable(f'Error getting logo')
    else:
        r.raise_for_status()
        if r.status_code == 200:
            log.info(f"Eventual logo url {eventual_logo_url}")
            return eventual_logo_url
        else:  # Status code is not an error, but not 200/OK
            log.warning(f"Http status code for {eventual_logo_url} was {r.status_code} ")
            return None


# TODO not called by tests
def get_not_found_logo_file():
    global not_found_file
    if not_found_file is None:
        not_found_filepath = current_dir + "../resources/1x1-00000000.png"
        log.debug(f'Opening global file: {not_found_filepath}')
        not_found_file = open(not_found_filepath, 'rb')
        not_found_file = not_found_file.read()
    return not_found_file


def fetch_ad_logo(ad_id):
    if settings.COMPANY_LOGO_FETCH_DISABLED:  # TODO: not executed by tests
        logo_url = None
    else:
        logo_url = get_correct_logo_url(ad_id)

    if logo_url is None:  # TODO not executed by tests
        log.info("Logo url not found, sending empty image")
        return file_formatter(get_not_found_logo_file())
    else:
        try:
            r = requests.get(logo_url, stream=True, timeout=5)
        except requests.RequestException as e:
            """
            If there is an exception (requests.RequestException is base for several different exceptions),
            log it and raise http 503 "Service Unavailable"
            """
            log.error(f"Error for logo url {logo_url}: {e}")
            raise ServiceUnavailable(f'Error getting logo')
        if r.status_code != 200:
            log.error(f"Status code {r.status_code} for {logo_url}")
            r.raise_for_status()
        log.info(f"Logo found on: {logo_url}")
        return file_formatter(r.raw.read(decode_content=False))


def file_formatter(file_object):
    return send_file(
        io.BytesIO(file_object),
        attachment_filename='logo.png',
        mimetype='image/png'
    )
