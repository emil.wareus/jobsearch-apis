import logging
import json
import time
from flask_restx import abort
from common import settings
from common.main_elastic_client import elastic_client
from operator import itemgetter
from common.elastic_connection_with_retries import elastic_search_with_retry
from sokannonser.repository.platsannonser import find_platsannonser, transform_platsannons_query_stats_result

log = logging.getLogger(__name__)


def suggest(args, querybuilder):
    """
    Returns a list of typeahead suggestions (in result['aggs']).
    In the normal case find_platsannonser() gives the suggestions in the aggs,
    which is then combined with the prefix words. However, <location><space>
    is a special case when instead extra words of type occupation are added.
    Finally, if the list of suggestions is still empty, then
    _complete_suggest(), and possibly _phrase_suggest(), will be called.

    The parameter CONTEXTUAL_TYPEAHEAD changes the behaviour of find_platsannonser()
    The suggestions and their ordering become different.
    True -> aggs based on subset of ads, from query match with prefix words
    False -> aggs based on all ads
    """
    prefix_words = args[settings.FREETEXT_QUERY]
    if (
            len(prefix_words.split()) == 1
            and args[settings.TYPEAHEAD_QUERY][-1:] == ' '
            and (word_type := _check_search_word_type(prefix_words, querybuilder)) == 'location'
    ):
        # is <location> + <space>: find extra words
        log.debug("<location><space> query pattern. Extra words used as typeahead result.")
        extra_words = suggest_extra_word(prefix_words, querybuilder, word_type)
        result = {'aggs': extra_words}
    else:
        result = find_platsannonser(args, querybuilder, start_time=0, x_fields=None)
        if result.get('aggs') and prefix_words:
            final_aggs = []
            for item in result.get('aggs'):
                value = item['value']
                if is_unwanted_suggested_word(value, args[settings.TYPEAHEAD_QUERY][-1:]):
                    continue
                item['value'] = prefix_words + ' ' + value
                item['found_phrase'] = prefix_words + ' ' + value
                final_aggs.append(item)
            result['aggs'] = final_aggs
        log.debug(f"Typeahead result (normal case): {result.get('aggs')}")
    if not result.get('aggs'):
        result = _complete_suggest(args, querybuilder, start_time=0)
        log.debug(f"No aggs found, result (complete suggest): {result.get('aggs')}")
        if not result.get('aggs'):
            result = _phrase_suggest(args, querybuilder, start_time=0)
            log.debug(f"No aggs and no complete_suggest, result (phrase suggest): {result.get('aggs')}")
    return result


def is_unwanted_suggested_word(value, last_word='*'):
    return value in settings.UNWANTED_SUGGESTED_WORDS and not any(
        [item.startswith(last_word) for item in settings.UNWANTED_SUGGESTED_WORDS])


def suggest_extra_word(search_text, querybuilder, search_text_type=None):
    # input one word and suggest extra word
    if not search_text_type:
        search_text_type = _check_search_word_type(search_text, querybuilder)
    new_suggest_list = []
    if search_text_type:
        second_suggest_type = 'occupation' if search_text_type == 'location' else 'location'
        query_dsl = querybuilder.create_suggest_extra_word_query(search_text, search_text_type, second_suggest_type)
        log.debug(f'(suggest_extra_word) query: {query_dsl}')
        query_result = elastic_search_with_retry(elastic_client(), query_dsl, settings.ES_INDEX)
        results = query_result.get('aggregations').get('first_word').get('second_word').get('buckets')
        for result in results:
            if is_unwanted_suggested_word(result.get('key')):
                continue
            new_suggest_list.append({
                'value': search_text + ' ' + result.get('key'),
                'found_phrase': search_text + ' ' + result.get('key'),
                'type': search_text_type + '_' + second_suggest_type,
                'occurrences': result.get('doc_count')
            })

    log.debug(f'(suggest_extra_word) extra words result: {new_suggest_list}')
    return new_suggest_list


def _check_search_word_type(word, querybuilder):
    # this function is used for checking input words type, return type location/skill/occupation
    query_dsl = querybuilder.create_check_search_word_type_query(word)
    log.debug(f'QUERY word_type: {query_dsl}, for word: {word}')
    query_result = elastic_search_with_retry(elastic_client(), query_dsl, settings.ES_INDEX)
    word_type = None
    if query_result:
        result = query_result.get('aggregations', {})
        for key in result.keys():
            if result[key]['buckets']:
                word_type = key.split('_')[-1]
                log.debug(f'Word type: {word_type}, for word: {word}')
                break
    return word_type


def _complete_suggest(args, querybuilder, start_time=0):
    if start_time == 0:
        start_time = int(time.time() * 1000)

    input_words = args.get(settings.TYPEAHEAD_QUERY)

    word_list = input_words.split()
    args_middle = args.copy()
    prefix_words = word_list[:-1] if word_list else ''
    if prefix_words:
        args_middle[settings.TYPEAHEAD_QUERY] = ' '.join(prefix_words)
        result = find_platsannonser(args_middle, querybuilder, start_time=0, x_fields=None)
        if not result.get('aggs'):
            return result

    word = word_list[-1] if word_list else ''
    if word_list and word_list[:-1]:
        prefix = ' '.join(input_words.split()[:-1])
    else:
        prefix = ''

    query_dsl = querybuilder.create_auto_complete_suggester(word)
    log.debug(f"(complete_suggest) args(complete_suggest): {args}")
    log.debug(f"(complete_suggest) query: {json.dumps(query_dsl)}")
    log.debug(f"(complete_suggest) query constructed after: {int(time.time() * 1000 - start_time)} milliseconds")
    query_result = elastic_search_with_retry(elastic_client(), query_dsl, settings.ES_INDEX)
    log.debug(f"(complete_suggest) results after: {int(time.time() * 1000 - start_time)} milliseconds")
    if not query_result:
        abort(500, 'Failed to establish connection to database')
        return

    log.debug(f"(complete_suggest) took: {query_result.get('took', 0)}, timed_out: {query_result.get('timed_out', '')}")

    aggs = []
    suggests = query_result.get('suggest', {})
    log.debug(f"(complete_suggest) query result: {suggests}")

    for key in suggests:
        if suggests[key][0].get('options', []):
            for ads in suggests[key][0]['options']:
                value = prefix + ' ' + ads.get('text', '') if prefix else ads.get('text', '')
                aggs.append(
                    {
                        'value': value.strip(),
                        'found_phrase': value.strip(),
                        'type': key.split('-')[0],
                        'occurrences': 0
                    }
                )

    # check occurrences even i think it will take some trouble and stupid
    query_result['aggs'] = _suggest_check_occurrence(aggs[:50], args, querybuilder)

    return query_result


def _phrase_suggest(args, querybuilder, start_time=0):
    if start_time == 0:
        start_time = int(time.time() * 1000)

    input_words = args.get(settings.TYPEAHEAD_QUERY)
    query_dsl = querybuilder.create_phrase_suggester(input_words)

    log.debug(f"(phrase_suggest) query constructed after: {int(time.time() * 1000 - start_time)} milliseconds.")
    log.debug(f"(phrase_suggest) args: {args}")
    log.debug(f"(phrase_suggest) query: {json.dumps(query_dsl)}")

    query_result = elastic_search_with_retry(elastic_client(), query_dsl, settings.ES_INDEX)
    log.debug(f"(phrase_suggest) results after: {int(time.time() * 1000 - start_time)} milliseconds.")
    if not query_result:
        abort(500, 'Failed to establish connection to database')
        return

    log.debug(f"(phrase_suggest) took: {query_result.get('took', 0)}, timed_out: {query_result.get('timed_out', '')}")

    aggs = []
    suggests = query_result.get('suggest', {})
    log.debug(f"(phrase_suggest) query result: {suggests}")

    for key in suggests:
        if suggests[key][0].get('options', []):
            for ads in suggests[key][0]['options']:
                value = ads.get('text', '')
                aggs.append(
                    {
                        'value': value,
                        'found_phrase': value,
                        'type': key.split('.')[-1].split('_')[0],
                        'occurrences': 0
                    }
                )

    # check occurrences even i think it will take some trouble and stupid
    query_result['aggs'] = _suggest_check_occurrence(aggs[:50], args, querybuilder)

    return query_result


def _suggest_check_occurrence(aggs, args, querybuilder):
    saved_value = args.get(settings.FREETEXT_QUERY)
    if args.get(settings.CONTEXTUAL_TYPEAHEAD, True):
        aggs = _suggest_check_occurrence_contextual(aggs, args, querybuilder)
    else:
        aggs = _suggest_check_occurrence_noncontextual(aggs, args, querybuilder)
    args[settings.FREETEXT_QUERY] = saved_value
    aggs = sorted(aggs, key=itemgetter('occurrences'), reverse=True)
    return aggs


def _suggest_check_occurrence_contextual(aggs, args, querybuilder):
    # check the frequency one by one, future will change it
    for agg in aggs:
        args[settings.FREETEXT_QUERY] = agg.get('value')
        query_dsl = querybuilder.parse_args(args)
        query_dsl['track_total_hits'] = True
        query_dsl['track_scores'] = True
        del query_dsl['aggs']
        query_result = elastic_search_with_retry(elastic_client(), query_dsl, settings.ES_INDEX)
        occurrences = query_result.get('hits').get('total').get('value')
        agg['occurrences'] = occurrences
    return aggs


def _suggest_check_occurrence_noncontextual(aggs, args, querybuilder):
    # check the frequency one by one, future will change it
    for agg in aggs:
        args[settings.TYPEAHEAD_QUERY] = agg.get('value')
        args[settings.FREETEXT_QUERY] = ""
        query_dsl = querybuilder.parse_args(args)
        query_result = elastic_search_with_retry(elastic_client(), query_dsl, settings.ES_INDEX)
        results = query_result.get('hits', {})
        results['took'] = query_result.get('took', 0)
        results['concepts'] = query_result.get('concepts', {})

        final_result = transform_platsannons_query_stats_result(args, query_result, querybuilder, results)
        for item in final_result.get('aggs', []):
            if item.get('value') == agg.get('value'):
                agg['occurrences'] = item.get('occurrences')
    return aggs
