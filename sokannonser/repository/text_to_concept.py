import logging
import time
from common import settings
from sokannonser.repository.ontology import Ontology
from common.elastic_connection_with_retries import create_elastic_client_with_retry
from sokannonser.repository.helpers import clean_plus_minus

log = logging.getLogger(__name__)

OP_NONE = ''
OP_PLUS = '+'
OP_MINUS = '-'


class TextToConcept(object):
    COMPETENCE_KEY = 'KOMPETENS'
    OCCUPATION_KEY = 'YRKE'
    TRAIT_KEY = 'FORMAGA'
    LOCATION_KEY = 'GEO'
    REMOVED_TAG = '<removed>'

    def __init__(self, ontologyhost='127.0.0.1', ontologyport=9200,
                 ontologyindex=settings.ONTOLOGY_INDEX, ontologyuser=None, ontologypwd=None):
        log.info("Creating Ontology elastic client")
        self.client = create_elastic_client_with_retry(ontologyhost, ontologyport, ontologyuser, ontologypwd)
        self.ontologyindex = ontologyindex
        self.ontology = None
        if not settings.DELAY_ONTOLOGY_STARTUP:
            self.get_ontology()

    def get_ontology(self):
        if self.ontology is None:
            start_time = int(time.time() * 1000)
            log.info(f'Creating Ontology, ontologyindex: {self.ontologyindex}')
            self.ontology = Ontology(client=self.client,
                                     index=self.ontologyindex,
                                     annons_index=settings.ES_INDEX,
                                     concept_type=None,
                                     include_misspelled=True)
            log.info(f"Ontology created after: {int(time.time() * 1000 - start_time)} ms")

        return self.ontology

    def text_to_concepts(self, text):
        # Note: Remove eventual '+' and '-' in every freetext query word since flashText is
        # configured so it can't find words starting with minus/hyphen.
        search_text = clean_plus_minus(text)
        text_lower = text.lower()
        ontology_concepts_orig = self.get_ontology().get_concepts(search_text, concept_type=None, span_info=True)
        ontology_concepts = [c[0] for c in ontology_concepts_orig]
        log.debug(f'Text: {text} ontology_concepts: {ontology_concepts}')
        text_lower_plus_blank_end = text_lower + ' '

        for concept in ontology_concepts:
            concept_term = concept['term']
            negative_concept_term = '-' + concept_term + ' '
            if ' ' + negative_concept_term in text_lower_plus_blank_end or \
                    text_lower_plus_blank_end.startswith(negative_concept_term):
                concept['operator'] = OP_MINUS
            elif '+' + concept_term + ' ' in text_lower_plus_blank_end:
                concept['operator'] = OP_PLUS
            else:
                concept['operator'] = OP_NONE

        skills = [c for c in ontology_concepts if self.filter_concepts(c, self.COMPETENCE_KEY, OP_NONE)]
        occupations = [c for c in ontology_concepts if self.filter_concepts(c, self.OCCUPATION_KEY, OP_NONE)]
        traits = [c for c in ontology_concepts if self.filter_concepts(c, self.TRAIT_KEY, OP_NONE)]
        locations = [c for c in ontology_concepts if self.filter_concepts(c, self.LOCATION_KEY, OP_NONE)]
        skills_must = [c for c in ontology_concepts if self.filter_concepts(c, self.COMPETENCE_KEY, OP_PLUS)]
        occupations_must = [c for c in ontology_concepts if self.filter_concepts(c, self.OCCUPATION_KEY, OP_PLUS)]
        traits_must = [c for c in ontology_concepts if self.filter_concepts(c, self.TRAIT_KEY, OP_PLUS)]
        locations_must = [c for c in ontology_concepts if self.filter_concepts(c, self.LOCATION_KEY, OP_PLUS)]
        skills_must_not = [c for c in ontology_concepts if self.filter_concepts(c, self.COMPETENCE_KEY, OP_MINUS)]
        occupations_must_not = [c for c in ontology_concepts if self.filter_concepts(c, self.OCCUPATION_KEY, OP_MINUS)]
        traits_must_not = [c for c in ontology_concepts if self.filter_concepts(c, self.TRAIT_KEY, OP_MINUS)]
        locations_must_not = [c for c in ontology_concepts if self.filter_concepts(c, self.LOCATION_KEY, OP_MINUS)]

        result = {'skill': skills,
                  'occupation': occupations,
                  'trait': traits,
                  'location': locations,
                  'skill_must': skills_must,
                  'occupation_must': occupations_must,
                  'trait_must': traits_must,
                  'location_must': locations_must,
                  'skill_must_not': skills_must_not,
                  'occupation_must_not': occupations_must_not,
                  'trait_must_not': traits_must_not,
                  'location_must_not': locations_must_not}

        return result

    @staticmethod
    def filter_concepts(concept, concept_type, operator):
        return concept['type'] == concept_type and concept['operator'] == operator
