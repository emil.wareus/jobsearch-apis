import os

API_VERSION = '1.23.0'

# Elasticsearch settings
ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")
ES_INDEX = os.getenv("ES_INDEX", "platsannons-read")
ES_STREAM_INDEX = os.getenv("ES_STREAM_INDEX", "platsannons-stream")

ES_TAX_INDEX_ALIAS = os.getenv("ES_TAX_INDEX_ALIAS", "taxonomy")
ES_RETRIES = int(os.getenv("ES_RETRIES", 1))
ES_RETRIES_SLEEP = int(os.getenv("ES_RETRIES_SLEEP", 2))

ES_HISTORICAL_TIMEOUT = int(os.getenv("ES_HISTORICAL_TIMEOUT", 300))
ES_DEFAULT_TIMEOUT = int(os.getenv("ES_DEFAULT_TIMEOUT", 10))

DELAY_ONTOLOGY_STARTUP = os.getenv('DELAY_ONTOLOGY_STARTUP', 'false').lower() == 'true'

# APM and Debug settings
APM_SERVICE_NAME = os.getenv("APM_SERVICE_NAME")
APM_SERVICE_URL = os.getenv("APM_SERVICE_URL")
APM_SECRET = os.getenv("APM_SECRET")
APM_LOG_LEVEL = os.getenv("APM_LOG_LEVEL", "WARNING")

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = False
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# Base API URL
BASE_PB_URL = os.getenv('BASE_PB_URL', 'https://arbetsformedlingen.se/platsbanken/annonser/')
BASE_TAXONOMY_URL = os.getenv('BASE_TAXONOMY_URL', 'https://taxonomy.api.jobtechdev.se/v1/taxonomy/')

TAXONOMY_APIKEY = os.getenv('TAXONOMY_APIKEY')

COMPANY_LOGO_BASE_URL = os.getenv('COMPANY_LOGO_BASE_URL',
                                  'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/')
COMPANY_LOGO_FETCH_DISABLED = os.getenv('COMPANY_LOGO_FETCH_DISABLED', 'false').lower() == 'true'
# Header parameters
APIKEY = 'api-key'
# Feature toggles
X_FEATURE_FREETEXT_BOOL_METHOD = 'x-feature-freetext-bool-method'
X_FEATURE_DISABLE_SMART_FREETEXT = 'x-feature-disable-smart-freetext'
X_FEATURE_ENABLE_FALSE_NEGATIVE = 'x-feature-enable-false-negative'

# Query parameters
OFFSET = 'offset'
LIMIT = 'limit'
FREETEXT_QUERY = 'q'
TYPEAHEAD_QUERY = 'typehead'
CONTEXTUAL_TYPEAHEAD = 'contextual'
UNSPECIFIED_SWEDEN_WORKPLACE = 'unspecified-sweden-workplace'
ABROAD = 'abroad'
REMOTE = 'remote'
TRAINEE = 'trainee'
LARLING = 'larling'
FRANCHISE = 'franchise'
HIRE_WORK_PLACE = 'hire-work-place'
FREETEXT_FIELDS = 'qfields'
SORT = 'sort'
PUBLISHED_BEFORE = 'published-before'
PUBLISHED_AFTER = 'published-after'
EXPERIENCE_REQUIRED = 'experience'
STATISTICS = 'stats'
STAT_LMT = 'stats.limit'
PARTTIME_MIN = 'parttime.min'
PARTTIME_MAX = 'parttime.max'
POSITION = 'position'
POSITION_RADIUS = 'position.radius'
DEFAULT_POSITION_RADIUS = 5
EMPLOYER = 'employer'
OPEN_FOR_ALL = 'open_for_all'

HISTORICAL_FROM = 'historical-from'
HISTORICAL_TO = 'historical-to'
HISTORICAL_REQUEST_TIMEOUT = 'request-timeout'
START_SEASONAL_TIME = 'start-seasonal-time'
END_SEASONAL_TIME = 'end-seasonal-time'
HISTORICAL_START_YEAR = int(os.getenv("HISTORICAL_START_YEAR", 2006))

OCCUPATION = 'occupation-name'
OCCUPATION_GROUP = 'occupation-group'
OCCUPATION_FIELD = 'occupation-field'
MUNICIPALITY = 'municipality'
DEFAULT_FREETEXT_BOOL_METHOD = 'or'

MAX_OFFSET = 2000
MAX_LIMIT = 100
MAX_TAXONOMY_LIMIT = 8000
MAX_COMPLETE_LIMIT = 50

RESULT_MODEL = 'resultmodel'
DETAILS = 'resdet'
MIN_RELEVANCE = 'relevance-threshold'

# For taxonomy
SHOW_COUNT = 'show-count'
TAX_DESCRIPTION = 'DEPRECATED, use https://taxonomy.api.jobtechdev.se/v1/taxonomy/swagger-ui/index.html instead'

# For Jobstream
DATE = 'date'
UPDATED_BEFORE_DATE = 'updated-before-date'
MAX_DATE = '3000-01-01T00:00:00'
OCCUPATION_CONCEPT_ID = 'occupation-concept-id'
LOCATION_CONCEPT_ID = 'location-concept-id'
OCCUPATION_LIST = ['occupation', 'occupation_field', 'occupation_group']
LOCATION_LIST = ['region', 'country', 'municipality']

# For all ads
SHOW_EXPIRED = 'show-expired'
API_KEY_RATE_LIMIT = None if os.getenv("API_KEY_RATE_LIMIT") == 'UNLIMITED' else os.getenv("API_KEY_RATE_LIMIT", 60)

result_models = [
    'pbapi', 'simple'
]

# sweden country concept id: /v1/taxonomy/main/concepts?id=i46j_HmG_v64'
SWEDEN_CONCEPT_ID = 'i46j_HmG_v64'

# original index: narvalontology, new: jae-synonym-dictionary
ONTOLOGY_INDEX = os.getenv('ONTOLOGY_INDEX', 'jae-synonym-dictionary')

UNWANTED_SUGGESTED_WORDS = ['sverige', 'svenska']

DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"

TAXONOMY_TYPE = 'taxonomy-type'
STATS_BY = 'stats-by'
LEGANCY_ID = 'legacy_ams_taxonomy_id'
CONCEPT_ID = 'concept_id'
LABEL = 'label'

LIMIT = 'limit'

TAXONOMY_EXTRAL_TYPE_LIST = ['worktime-extent',  'wage-type', 'sun-education-field-1', 'sun-education-field-2',
                             'sun-education-field-3', 'sun-education-level-1', 'sun-education-level-2',
                             'sun-education-level-3', 'employment-duration']

TAXONOMY_TYPE_LIST = ['occupation-name', 'occupation-group', 'occupation-field', 'employment-type', 'country',
                      'region', 'municipality', 'language', 'skill']
TAXONOMY_GENERAL_TYPE_LIST = ['occupation-name', 'occupation-group', 'occupation-field', 'employment-type']
TAXONOMY_LOCATION_TYPE_LIST = ['country', 'region', 'municipality']
TAXONOMY_TYPE_MUST_HAVE_LIST = ['language', 'skill']


TAXONOMY_PROCESSES = int(os.getenv("TAXONOMY_PROCESSES", 8))
