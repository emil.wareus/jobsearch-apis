import time


def calculate_utc_offset():
    is_dst = time.daylight and time.localtime().tm_isdst > 0
    utc_offset = - (time.altzone if is_dst else time.timezone)
    return int(utc_offset / 3600) if utc_offset > 0 else 0
