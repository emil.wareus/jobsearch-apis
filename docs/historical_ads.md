# Historical Ads API

## Running the app

To run the Historical Ads API, set FLASK_APP=historical

We do not have real historical data yet, 
so you can let ES_INDEX be set to your usual platsannonser index.

And if you would use an old index you will already see that the Historical app will show all ads,
unlike the Jobsearch app that will only show currently published ads.

## Editing the app

Most of the code is shared between Historical and Jobsearch. 
The following files are of special interest when building functionality for Historical:

### historical.py
Starts the Flask app.


### sokannonser/rest/endpoint/historical.py
Defines the endpoints for Historical.

### sokannonser/rest/endpoint/common.py
Endpoint functions that are common between Jobsearch and Historical.

### sokannonser/rest/model/apis.py
Definition of the APIs for both Jobsearch and Historical. Three sections:
1. First the root_api is augmented with all models that are common 
   to Jobsearch and Historical, but do not belong in Jobstream.
2. Then API for Jobsearch is created, adding parameters that is unique for Jobsearch
3. Finally the Historical API is created.
    
### sokannonser/rest/model/queries.py
Here the endpoint query fields are defined:
* **base_annons_query** - common for Jobsearch and Historical
* **annons_complete_query** - for Jobsearch /complete
* **annons_search_query** - for Jobsearch /search
* **annons_complete_query** - for Historical /search

### sokannonser/repository/querybuilder.py
The same query builder is used both for Jobsearch and Historical,
but the behaviour will differ depending on app and endpoint - especially concerning 'aggs'.
(More to come about how we will deal with this)
