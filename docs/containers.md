# Running a Jobsearch Container

**Note:** This might not be fully correct for current `Dockerfile`.

## Linux and WSL 2

These instructions shall work on Linux and on WSL 2 with podman installed.
If you not run podman, replace `podman` with `docker` and it shall work.

### Start Elasticsearch

Unless you already have an Elasticsearch process running locally with a 
jobads index, you may use the following "naked" approach:

Start an instance of Elasticsearch:
```shell
podman run --rm -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elastic docker.io/library/elasticsearch:7.13.2
```

Create indices in Elasticsearch:
```shell
curl -X PUT http://$(hostname -I | xargs):9200/jae-synonym-dictionary
curl -X PUT http://$(hostname -I | xargs):9200/platsannons-read
```

If you run both IPv4 and IPv6, `hostname -I` might return two addresses. Then just
replace `$(hostname -I | xargs)` with your primary Ipv4 address.

### Start Jobsearch

In another terminal window build and start jobsearch:
```shell
podman build -t jobsearch:dev .
podman run -e ES_HOST=$(hostname -I) -p 8081:8081 --name=jobsearch --rm jobsearch:dev
```

Now you can point your browser on the IP `hostname -I` shows and 
port 8081, example `http://172.18.9.180:8081/`. It will show the 
API-documentation.

If you want to run jobstream do:
```
podman run -e ES_HOST=$(hostname -I) -e FLASK_APP=jobstream -p 8082:8081 --name=jobstream --rm jobsearch:dev
```

To be able to run the two apps in parallel, `jobstream` is bound to port 8082 on the host.
Example `http://172.18.9.180:8082/`. It will show the 
API-documentation.

To stop the containers:
```shell
podman kill jobsearch jobstream elastic
```

## MAC

These instructions shall work on Mac with docker installed.

Turn off VPN!

### Start Elasticsearch

Unless you already have an Elasticsearch process running locally with a 
jobads index, you may use the following "naked" approach:

Start an instance of Elasticsearch:
```shell
docker run --rm -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" --name elastic docker.io/library/elasticsearch:7.13.2
```

Create indices in Elasticsearch:
```shell
curl -X PUT http://localhost:9200/jae-synonym-dictionary
curl -X PUT http://localhost:9200/platsannons-read
```

### Start Jobsearch

In another terminal window build and start jobsearch:
```shell
docker build -t jobsearch:dev .
docker run -e ES_HOST=host.docker.internal -p 8081:8081 --name=jobsearch --rm jobsearch:dev
```

The API is accessible at http://0.0.0.0:8081, and also at http://localhost:8081.

To run the `jobstream` app instead, do:
```
docker run -e ES_HOST=host.docker.internal -e FLASK_APP=jobstream -p 8082:8081 --name=jobstream --rm jobsearch:dev
```
To be able to run the two apps in parallel, `jobstream` is bound to port 8082 on the host.
Example `http://127.0.0.1:8082/`. It will show the API-documentation.

To stop the containers:
```shell
docker kill jobsearch jobstream elastic
```
