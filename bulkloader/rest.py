import logging
from datetime import datetime, timedelta
from flask import Response
from flask_restx import Resource
from jobtech.common.rest.decorators import check_api_key_and_return_metadata
from common import settings
from bulkloader.repository import load_all, load_snapshot
from bulkloader.api import ns_bulk, bulk_stream_query, bulk_snapshot_query
import elasticapm

log = logging.getLogger(__name__)


@ns_bulk.route('stream')
class BulkLoad(Resource):
    method_decorators = [check_api_key_and_return_metadata('bulk', settings.API_KEY_RATE_LIMIT)]
    example_date = (datetime.now() - timedelta(minutes=10)).strftime(settings.DATE_FORMAT)

    @ns_bulk.doc(
        description="Download all the ads that has been changed (e.g. added, updated, unpublished) during requested time interval. "
                    "Rate limit is one request per minute.",
        params={
            settings.APIKEY: "Use the temporary api-key: `api-awards-key` to test",
            settings.DATE: "Stream ads changed since datetime. "
                           "Accepts datetime as YYYY-MM-DDTHH:MM:SS, "
                           "for example %s." % example_date,
            settings.UPDATED_BEFORE_DATE: "Stream ads changed before datetime. "
                                          "Accepts datetime as YYYY-MM-DDTHH:MM:SS. "
                                          "Optional if you want to set a custom time span. "
                                          "Defaults to 'now' if not set.",
            settings.OCCUPATION_CONCEPT_ID: "Filter stream ads by one or more occupation concept ids "
                                            "(from occupation, occupation_group, occupation_field).",
            settings.LOCATION_CONCEPT_ID: "Filter stream ads by one or more location concept ids "
                                          "(from municipality_concept_id, region_concept_id, country_concept_id)."
        },
        responses={
            200: 'OK',
            401: 'Invalid API-key',
            429: 'Rate limit exceeded',
            500: 'Technical error'
        }
    )
    @ns_bulk.expect(bulk_stream_query)
    def get(self, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        args = bulk_stream_query.parse_args()
        log.info(f"ARGS: {args}")
        return Response(load_all(args), mimetype='application/json')


@ns_bulk.route('snapshot')
class SnapshotLoad(Resource):
    method_decorators = [check_api_key_and_return_metadata('bulk', settings.API_KEY_RATE_LIMIT)]

    @ns_bulk.doc(
        description="Download all the ads currently published in Platsbanken. "
                    "The intended usage for this endpoint is to make an initial copy of the job ad dataset "
                    "and then use the stream endpoint to keep it up to date. "
                    "Avoid using in Swagger your browser will probably hang",
        responses={
            200: 'OK',
            401: 'Invalid API-key',
            429: 'Rate limit exceeded',
            500: 'Technical error'
        }
    )
    @ns_bulk.expect(bulk_snapshot_query)
    def get(self, **kwargs):
        elasticapm.set_user_context(username=kwargs.get('key_app'), user_id=kwargs.get('key_id'))
        args = bulk_snapshot_query.parse_args()
        log.info(f"ARGS: {args}")
        return Response(load_snapshot(), mimetype='application/json')
