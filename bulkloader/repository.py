import datetime
import logging
import json
import time
from datetime import date, timedelta
from elasticsearch.helpers import scan
from common import settings
from common.main_elastic_client import elastic_client
from common.helpers import calculate_utc_offset
from common.result_model import job_ad
from bulkloader.api import ns_bulk

log = logging.getLogger(__name__)

offset = calculate_utc_offset()


def _es_dsl():
    dsl = {
        "query": {
            "bool": {
                "filter": [
                    {
                        "range": {
                            "publication_date": {
                                "lte": "now/m+%dH/m" % offset
                            }
                        }
                    },
                    {
                        "range": {
                            "last_publication_date": {
                                "gte": "now/m+%dH/m" % offset
                            }
                        }
                    }
                ]
            }
        },
    }
    return dsl


def _index_exists(idx_name):
    return elastic_client().indices.exists_alias(name=[idx_name]) or elastic_client().indices.exists(index=[idx_name])


# Generator function
def load_all(args):
    since = args.get(settings.DATE)
    # time span, default is None
    if args.get(settings.UPDATED_BEFORE_DATE, None):
        before = args.get(settings.UPDATED_BEFORE_DATE)
    else:
        before = datetime.datetime.strptime(settings.MAX_DATE, '%Y-%m-%dT%H:%M:%S')

    # input is not allowed by type=inputs.datetime_from_iso8601
    if since == 'yesterday':
        since = (date.today() - timedelta(1)).strftime('%Y-%m-%d')

    ts_from = int(time.mktime(since.timetuple())) * 1000
    # add 1 sec to find ad (ms truncation problem)
    ts_to = int(time.mktime(before.timetuple()) + 1) * 1000

    if _index_exists(settings.ES_STREAM_INDEX):
        index = settings.ES_STREAM_INDEX
    else:
        log.warning(f"Elastic stream index: {settings.ES_STREAM_INDEX} does not exist")
        index = settings.ES_INDEX
    log.info(f"(load_all)Elastic stream index is set to: {index}")

    dsl = _es_dsl()
    dsl['query']['bool']['must'] = [{
        "range": {
            "timestamp": {
                "gte": ts_from,
                "lte": ts_to
            }
        }
    }]

    occupation_concept_ids = args.get(settings.OCCUPATION_CONCEPT_ID)
    if occupation_concept_ids:
        occupation_list = [occupation + '.' + 'concept_id.keyword' for occupation in settings.OCCUPATION_LIST]
        add_filter_query(dsl, occupation_list, occupation_concept_ids)

    location_concept_ids = args.get(settings.LOCATION_CONCEPT_ID)
    if location_concept_ids:
        location_list = ['workplace_address.' + location + '_concept_id' for location in settings.LOCATION_LIST]
        add_filter_query(dsl, location_list, location_concept_ids)

    log.info(f"(load_all)Query: {json.dumps(dsl)}")

    scan_result = scan(elastic_client(), dsl, index=index)
    counter = 0
    yield '['
    for ad in scan_result:
        if counter > 0:
            yield ','
        source = ad['_source']
        if source.get('removed', False):
            yield json.dumps(format_removed_ad(source))
        else:
            yield json.dumps(format_ad(source))
        counter += 1
    log.info(f"(load_all)Delivered: {counter} ads as stream")
    yield ']'


def add_filter_query(dsl, items, concept_ids):
    # add occupation or location filter query

    should_query = []
    for concept_id in concept_ids:
        if concept_id:
            for item in items:
                should_query.append({"term": {
                    item: concept_id
                }})
    dsl['query']['bool']['filter'].append({"bool": {"should": should_query}})
    return dsl


@ns_bulk.marshal_with(job_ad)
def format_ad(ad_data):
    ad_data['employment_type'] = get_original_value_from_list(ad_data.get('employment_type', None))
    ad_data['occupation'] = get_original_value_from_list(ad_data.get('occupation', None))
    if ad_data.get('occupation_group', None):
        ad_data['occupation_group'] = ad_data.get('occupation_group', None)[0]
    else:
        ad_data['occupation_group'] = {
            "concept_id": None,
            "label": None,
            "legacy_ams_taxonomy_id": None}
    if ad_data.get('occupation_field', None):
        ad_data['occupation_field'] = ad_data.get('occupation_field', None)[0]
    else:
        ad_data['occupation_field'] = {
            "concept_id": None,
            "label": None,
            "legacy_ams_taxonomy_id": None}
    return ad_data


def get_original_value_from_list(full_list, only_concept_id=False):
    if not full_list:
        return {
            "concept_id": None,
            "label": None,
            "legacy_ams_taxonomy_id": None}

    # workaround until only original value is added by importer
    if isinstance(full_list, dict):
        return {
            "concept_id": full_list.get('concept_id'),
            "label": full_list.get('label'),
            "legacy_ams_taxonomy_id": full_list.get('legacy_ams_taxonomy_id')}

    for value in full_list:
        if value.get('original_value', False):
            if only_concept_id:
                return value.get('concept_id')
            else:
                return {
                    "concept_id": value.get('concept_id'),
                    "label": value.get('label'),
                    "legacy_ams_taxonomy_id": value.get('legacy_ams_taxonomy_id')}
    return None


# @marshaller.marshal_with(removed_job_ad)
def format_removed_ad(ad_data):
    return {
        'id': str(ad_data.get('id')),
        'removed': ad_data.get('removed'),
        'removed_date': ad_data.get('removed_date'),
        'occupation': ad_data.get('occupation', None),
        'occupation_group': ad_data.get('occupation_group', None),
        'occupation_field': ad_data.get('occupation_field', None),
        'municipality': ad_data.get('workplace_address', {}).get('municipality_concept_id', None),
        'region': ad_data.get('workplace_address', {}).get('region_concept_id', None),
        'country': ad_data.get('workplace_address', {}).get('country_concept_id', None)
    }


def load_snapshot():
    if _index_exists(settings.ES_STREAM_INDEX):
        index = settings.ES_STREAM_INDEX
    else:
        log.warning(f"Elastic stream index: {settings.ES_STREAM_INDEX} does not exist")
        index = settings.ES_INDEX
    log.info(f"(load_snapshot)Elastic stream index is set to: {index}")

    dsl = _es_dsl()
    dsl['query']['bool']['filter'].append({"term": {"removed": False}})
    log.info(f"QUERY(load_snapshot): {json.dumps(dsl)}")
    scan_result = scan(elastic_client(), dsl, index=index)
    counter = 0
    yield '['
    for ad in scan_result:
        if counter > 0:
            yield ','
        source = ad['_source']
        yield json.dumps(format_ad(source))
        counter += 1
    log.info(f"(load_snapshot)Delivered: {counter} ads as stream")
    yield ']'
