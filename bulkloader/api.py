from datetime import datetime, timedelta
from flask_restx import Api, Namespace, reqparse, inputs
from common import settings
from common.result_model import root_api

#Including doc breaks it ,/doc='https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-apis/-/blob/main/docs/GettingStartedJobStreamEN.md'
stream_api = Api(version=settings.API_VERSION,
                 title='JobStream',
                 description='An API for retrieving all currently published job ads in Platsbanken',
                 default_label="An API for retrieving job ads",
                 contact_email='jobtechdev@arbetsformedlingen.se',
                 contact_url='https://jobtechdev.se',
                 contact='Jobtechdev',
                 license='CC0',
                 license_url='https://creativecommons.org/publicdomain/zero/1.0/deed.sv'
                )


ns_bulk = Namespace('Platsbanken ads', description='Start with a snapshot and keep it up to date with stream')

stream_api.add_namespace(ns_bulk, '/')

for name, definition in root_api.models.items():
    ns_bulk.add_model(name, definition)

default_time = (datetime.now() - timedelta(minutes=10)).strftime(settings.DATE_FORMAT)

bulk_stream_query = reqparse.RequestParser()
bulk_stream_query.add_argument(settings.APIKEY, location='headers', required=True)
bulk_stream_query.add_argument(settings.DATE, type=inputs.datetime_from_iso8601,
                               required=True, default=default_time)
bulk_stream_query.add_argument(settings.UPDATED_BEFORE_DATE, type=inputs.datetime_from_iso8601,
                               required=False)
bulk_stream_query.add_argument(settings.OCCUPATION_CONCEPT_ID, action='append', required=False)
bulk_stream_query.add_argument(settings.LOCATION_CONCEPT_ID, action='append', required=False)
bulk_snapshot_query = reqparse.RequestParser()
bulk_snapshot_query.add_argument(settings.APIKEY, location='headers', required=True)
